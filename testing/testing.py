# testing utility methods
import os

from utilities import graph_utils as g_util
from clone_detector import clone_detector as detector
from similarity_strategy import similarity_strategies as sim
import clone_commander
from clone_presenter import clone_renderer as presenter


GRAPH_DIR = '/projects/eecs493/clone_families/training_data/jgit_subset'
GRAPH_ARCHIVE = '/projects/eecs493/clone_families/training_data/jgit.tar.gz'
FAMILY_OUTPUT_DIR = '/projects/eecs493/clone_families/training_data/jgit_subset_clone_families'
THRESH_DIR = '/projects/eecs493/clone_families/training_data'

# set utility function
def peek(s):
    item=s.pop()
    s.add(item)
    return item

class mock_command:
    def __init__(self):
        self.time = False
        self.archive = GRAPH_ARCHIVE
        self.directory = GRAPH_DIR
        self.sim_metric = detector.LED
        self.visual = False

def test_all():
    # test the different algorithms
    commands = mock_command()
    metrics = [detector.KERNEL, detector.LED]
    for m in metrics:
        commands.sim_metric = m
        clone_commander.process_clone_detection_commands(commands)
        graph_db, graph_strings = clone_commander.build_graph_db(GRAPH_ARCHIVE, GRAPH_DIR, m)
        pred_families, time, scores = detector.find_clone_families(graph_db, m, True)

        thresh = 1 - 0.000000001 #0.000000001
        print_similar_graphs(graph_db, m, thresh, scores)

        #[pred_families, actual_families] = run_test_2(graph_db, m)
        #[tp, tn, fp, fn, prec, rec, acc, fm] = calculate_basic_metrics_per_cluster(pred_families, actual_families, graph_db)
        roc = calculate_cluster_rocs(pred_families, graph_db, m, scores)
        [intercm, intracm] = calculate_cluster_metrics(pred_families, graph_db, m, scores)

def summarize_test():
    pass


def calculate_basic_metrics_per_cluster(predicted_clusters, actual_clusters, graph_db):
    thresh_acc=0.9
    fweight=2
    tp, tn, fp, fn = [],[],[],[]
    prec = []
    rec = []
    acc = []
    fm = []
    nclusters = len(predicted_clusters)
    ngraphs = len(graph_db)
    all_graphs = set(range(ngraphs))
    for i in range(nclusters):
        tp.append(len(predicted_clusters[i].intersection(actual_clusters[i])))
        tn.append(len((all_graphs-predicted_clusters[i]).intersection(all_graphs-actual_clusters[i])))
        fp.append(len(predicted_clusters[i]) - tp[i])
        fn.append(len(actual_clusters[i]) - tp[i])
        len_pred = len(predicted_clusters[i])
        len_actual = len(actual_clusters[i])
        #precision: positive predictive value: measure of how well we minimize false positives = P(T/predT)
        prec.append(tp[i]/len_pred)
        #recall: true positive rate: measure of how well we minimize false negatives = P(predT|T)
        rec.append(tp[i]/len_actual)
        #accuracy:
        acc.append((tp[i]+tn[i])/(tp[i] + fp[i] + tn[i] + fn[i]))
        #false positive rate:
        #fpr = fp[i]/(fp[i] + tn[i])
        #fmeasure: weighted harmonic mean of precision and recall
        fm.append((1 + fweight*fweight)*(prec[i]*rec[i]) / (fweight*fweight*prec[i] + rec[i]))
    return [tp, tn, fp, fn, prec, rec, acc, fm]

def calculate_cluster_metrics(predicted_clusters, graph_db, metric, scores):
    #cluster metrics: 'means', 'variances', intra-cluster separation, Hubert statistic
    cluster_means = []
    cluster_variances = []
    for cluster in predicted_clusters:
        [m,v] = compute_inter_cluster_metrics(cluster, graph_db, metric, scores)
        cluster_means.append(m)
        cluster_variances.append(v)
    sep = compute_intra_cluster_metrics(predicted_clusters, cluster_means, cluster_variances, metric, scores)
    #compute the hubert statistic for the clustering scheme to characterize the degree of agreement between the clustering
    #and the similarity matrix
    ngraphs = len(graph_db)
    Y = []
    M = ngraphs * (ngraphs-1)/2 #number of pairs
    for i in range(ngraphs):
        Y.append([1 for j in range(ngraphs)])
    for clust in predicted_clusters:
        for c1 in clust:
            for c2 in clust:
                Y[c1][c2] = 0
    hub = 0
    for i in range(1,ngraphs):
        for j in range(i+1, ngraphs+1):
            hub += scores[i][j] * Y[i][j]
    hub = hub/float(M) #higher values of this are better
    intracm = [sep, hub]
    intercm = [cluster_means, cluster_variances]
    return [intercm, intracm]

def compute_intra_cluster_metrics(clusters, cluster_means, cluster_variances, metric, scores):
    ##compute the intra-cluster distances: compare cluster centriods (pattern graphs)
    sep = []
    for cm1 in cluster_means:
        s = []
        for cm2 in cluster_means:
            cmsep = 0
            if not cm2 == cm1:
                cmsep = 1 - scores[cm1][cm2]
            s.append(cmsep)
        sep.append(s) #separation matrix

    return sep

#need to define 'mean' and 'variance' in terms of groups of graphs...
#'variance' could be interpreted as (1 - the average normal similarity score between each pair of graphs in the group)^2
#'mean' could be interpreted as ... the 'pattern' graph generated for the cluster ?
def compute_inter_cluster_metrics(cluster, graph_db, metric, scores):
    [mean, variance] = 0,0
    #calculate the variance from the similarity scores
    for g1 in cluster:
        for g2 in cluster:
            if g1 == g2:
                continue
            variance += scores[g1][g2]
    variance = variance / len(cluster)
    variance = (1 - variance)**2
    #calculate the 'mean' from the pattern graph
    pattern_graph_path = g_util.get_pattern_graph_path(cluster, GRAPH_DIR) #dot file
    pattern_graph_string = util.read_file_contents(graph_file_path)
    pattern_graph = parse_simple_graph(graph_string)
    mean = pattern_graph
    return [mean, variance]



def calculate_cluster_rocs(predicted_clusters, graph_db, metric):
    ##metrics.roc_auc_score(actual_clusters[i], pred_clusters[i]
    #roc area: percentage of pairs (member, non-member) that are classified correctly
    roc = []
    ngraphs = len(graph_db)
    for predicted_cluster in predicted_clusters:
        nonmembers = set(range(ngraphs)) - set(predicted_cluster)
        cluster_size = len(predicted_cluster)
        nincorrect = 0
        npairs = (ngraphs - cluster_size) * cluster_size

        #for each member, nonmember pair...
        for gc in predicted_cluster:
            for gn in nonmembers:
                #calculate average similarity score between gc (gn) and predicted_cluster
                sim_gc = 0
                sim_gn = 0
                sim_gcs = []
                sim_gns = []
                for gp in predicted_cluster:
                    if gc == gp: continue #obviously gc will be more similar to itself than gn
                    #scores is a 2x2 symmetric matrix, S(i,j)e[0,1], S(i,i)=1
                    graph_db_gcgp = [graph_db[gc], graph_db[gp]]
                    graph_db_gngp = [graph_db[gn], graph_db[gp]]
                    scores_gc = sim.similarity_scores_from_graph_db(graph_db_gcgp, metric)
                    scores_gn = sim.similarity_scores_from_graph_db(graph_db_gngp, metric)
                    if gp == gc:
                        sim_gn += scores_gn[0][1]
                    elif gp == gn:
                        sim_gc += scores_gc[0][1]
                    else:
                        sim_gc += scores_gc[0][1]
                        sim_gn += scores_gn[0][1]
                    sim_gcs.append(sim_gc)
                    sim_gns.append(sim_gn)
                if not (sim_gc == 0 and sim_gn == 0):
                    sim_gc = float(sim_gc) / (cluster_size - 1)
                    sim_gn = float(sim_gn) / (cluster_size - 1)
                    if sim_gn > sim_gc:
                        nincorrect += 1
                    elif sim_gc == sim_gc:
                        nincorrect += 0.5
        roc.append((npairs-nincorrect)/npairs)
    return roc

def calculate_cluster_rocs_2(predicted_clusters, graph_db):
    cluster_rocs = []
    ngraphs = len(graph_db)
    all_graphs = set(range(ngraphs))
    for predicted_cluster in predicted_clusters:
        len_pred = len(predicted_cluster)
        nincorrect = 0
        npairs = (ngraphs - len_pred) * len_pred
        for gp in predicted_cluster:
            for gg in all_graphs - predicted_cluster:
                test_sim_score_gp = test_sim_wrt_cluster(gp, predicted_cluster, graph_db)
                test_sim_score_gg = test_sim_wrt_cluster(gg, predicted_cluster, graph_db)
                if test_sim_score_gg > test_sim_score_gp: nincorrect += 1
                elif test_sim_score_gg == test_sim_score_gp: nincorrect += 0.5
        cluster_rocs.append((npairs-nincorrect)/npairs)
    return cluster_rocs

#test how similar a graph is to a cluster using a backwards tree-traversal method?
def test_sim_wrt_cluster(gindex, cluster, graph_db):
    sim = 0
    for gc in cluster:
        sim += test_sim_wrt_pair(graph_db[gindex], graph_db[gc])
    return sim


def run_test_2(graph_db, metric):
    clone_families, time = detector.find_clone_families(graph_db, metric, True)
    fn=0
    correct_families = []
    for family in clone_families:
        nclones = len(family)
        correct_families.append(visual_compare(family))
        #correct_families.append(tree_compare(family))
        #correct_families.append(get_correct_family_from_db(family)
    return [clone_families, correct_families]


#compare each pair of graph patterns in a clone family using a graph-traversal algorithm to programmatically
#determine whether a graph pattern has been correctly classified.
def tree_compare(clone_family):
    pass

#compare two graphs based on a graph-traversal method
#g1 and g2 are graphs-- a graph is a list of triples
def test_sim_wrt_pair(g1, g2):
    sim = 0

    return sim

#compare each pair of graph patterns in a clone family visually to determine whether a graph pattern
#is correctly classified. This is an unideal method since it introduces subjective bias.
def visual_compare(clone_family):
    nclones = len(clone_family)
    ncorrect_clones = [0 for i in range(nclones)]
    list_family = [clone_family.pop() for i in range(nclones)]
    for i in range(nclones):
        base_output_dir_i = '/'.join([FAMILY_OUTPUT_DIR, str(i), str(list_family[i])])
        for j in range(i):
            base_output_dir_j = '/'.join([FAMILY_OUTPUT_DIR, str(i), str(list_family[j])])
            fh = os.popen('feh -w '+base_output_dir_i+'/pattern.png '+base_output_dir_j+'/pattern.png & disown')
            ans=raw_input('are the graphs similar? y/n >> ')
            while not (ans=='y' or ans=='n'):
                ans = raw_input('invalid response, try again >> ')
            if ans=='y':
                ncorrect_clones[j] += 1
    for i in range(nclones):
        if ncorrect_clones[i] == nclones:
            ncorrect_clones[i] = 1
        else:
            ncorrect_clones[i] = -1
    return set([ncorrect_clones[i]*list_family[i] for i in range(nclones) if ncorrect_clones[i]*list_family[i] > 0])

#determine what an actual family should be by looking through the entire graph database. This is somewhat infeasible,
#but the other methods only cover the case where a given pattern within a family should not belong to that family, and
#not the case where there exists a pattern currently not in the family that should belong to the family.
def get_correct_family_from_db(family):
    pass

def compareGraphs( gid, clusters):
    gfile='pattern.dot'
    files = os.popen('ls '+os.path.expanduser('~')+GRAPH_DIR+'/*/'+gfile).read().split()
    pidcmd='ps -C xdot | egrep -o "[0-9]{3,6}"'
    sim_graphs, dotcmds, dots, pids = [], [], [], [gid]
    for f in files:
        dotcmds.append('xdot '+f+' &')
    refdot = os.popen(dotcmds[gid])
    refpid = os.popen(pidcmd).read()
    ans=None
    while not ans:
        ans=raw_input('the reference graph is '+files[gid]+'. press any key to continue. >> ')
    valid_responses=['yes', 'y', 'no', 'n']
    for i in range(1, len(files)):
        if not i == gid:
            dots.append(os.popen(dotcmds[i]))
            pids.append(os.popen(pidcmd).read().split()[1])
            ans=raw_input('are the graphs similar? yes/y/no/n >> ')
            while ans not in valid_responses:
                ans = raw_input('invalid response, try again >> ')
            if ans=='yes' or ans=='y':
                #verify that graph has not already been selected for another group
                add = True
                for cluster in clusters:
                    if i in cluster:
                        print('Error! graph already belongs to a group.\n')
                        print('options:\n')
                        print('\t 0=>deselect\n')
                        print('\t 1=>remove from previous group\n')
                        print('\t 2=>view graphs from previous group before deciding\n')
                        ans = int(raw_input('>> '))
                        while not (ans == 0 or ans == 1 or ans == 2):
                            ans = int(raw_input('invalid option, try again >> '))
                        if ans == 0:
                            add = False
                        elif ans == 1:
                            cluster.discard(i)
                        elif ans == 2:
                            tempdots, temppids = [], []
                            for b in cluster:
                                if not b == i:
                                    tempdots.append(os.popen(dotcmds[b]))
                                    temppids.append(os.popen(pidcmd).read().split()[2])
                            print('0=>deselect graph '+i+' from current group\n')
                            print('1=>remove graph '+i+' from previous group\n')
                            ans = int(raw_input('>> '))
                            while not (ans == 0 or ans == 1): ans = int(raw_input('invalid option, try again >> '))
                            if ans == 0:
                                add = False
                            elif ans == 1:
                                cluster.discard(i)
                            for pid in temppids:
                                os.popen('kill -9 '+pid)
                        break
                if add: sim_graphs.append(i)
            os.popen('kill -9 '+pids[i])
        else:
            dots.append(refdot)
            pids.append(refpid)
    print('images similar to '+files[gid]+':\n')
    for index in sim_graphs:
        print('\t'+files[index]+'\n')
    os.popen('kill -9 '+pids[gid])
    clusters.append(sim_graphs)
    return clusters

def print_similar_graphs(graph_db, metric, thresh, scores):
    ngraphs = len(graph_db)
    similar_graph_mask = [0 for i in range(ngraphs)]
    for i in range(ngraphs):
        for j in range(ngraphs):
            if not i==j:
                if scores[i][j] > thresh:
                    similar_graph_mask[i] = 1
                    similar_graph_mask[j] = 1
    output_dir = os.path.expanduser('~') + THRESH_DIR + "/threshold_" + str(thresh)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    for i in range(ngraphs):
        if similar_graph_mask[i] == 1:
            pattern_output_file = output_dir + "/graph_" + str(i)
            pattern_graph_path = g_util.get_pattern_graph_path(i, os.path.expanduser("~") + GRAPH_DIR)
            presenter.render_dot_as_png(pattern_graph_path, pattern_output_file)

###from similarity_strategies.py###
#extensions to similarity strategies which take a graph_db (i.e., a list of 2 graphs), and return the similarity scores
#corresponding to the metric used. This will be useful in testing how well the clustering works, by facilitating the
#calculation of ROC areas (which is essentially a correspondence-metric between all member-nonmember pairs)
#def similarity_scores_from_graph_db(graph_db, metric):
#    scores = []
#    if metric is "Levenshtein":
#        scores = generate_similarity_scores(graph_db, graph_label_comparison)
#    elif metric is "Kernel":
#        scores = generate_similarity_scores_kernel(graph_db)
#    elif metric is "Kernel-rw":
#        labels=get_labels(graph_db)
#        label_adjacency_labels=[]
#        for graph in graph_db:
#            label_adjacency_labels.append(get_label_adjacency_label(graph,labels))
#        scores = normalize(generate_similarity_scores_kernel_rw(label_adjacency_labels))
#    return scores

if __name__ == '__main__': test_all()