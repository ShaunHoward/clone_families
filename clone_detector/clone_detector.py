__author__ = 'Shaun Howard'

from similarity_strategy import similarity_strategies as sim
from similarity_strategy import kmeans_strategy as kmeans
from similarity_strategy import kmedoids_strategy as kmedoids
import time

"""
A script to detect code clones in a maximal frequent sub-graph database
mined by Tim Henderson's (tim.tadh@gmail.com) custom frequent sub-graph mining software.

Some parameters for running the "find_clone_families" method of this script
along can be defined below.

The first parameter is a graph database. A graph is either a list of sde triples or a list of simple graphs.
Each triple represents a source node label, a destination node label, and an
edge node label in that graph. Each triple may or may not be unique or so the tool will
detect.

The second parameter is the method to use for code clone detection. The methods and
brief descriptions are the following:

"Levenshtein": Levenshtein Edit Distance (GLED)
Description: Informally, the Levenshtein distance between two words is the minimum number
of single-character edits (i.e. insertions, deletions or substitutions) required to change
one word into the other. We use the bliss source destination edge code representations of
graphs (BSDE codes) to compare all graphs in the graph database pairwise. We then group the
similar pairs using the transitive closure of the pairs by graph integer index. Each
resultant cluster contains the indices of similar graphs.

"K-means": K-means clustering
Description: K-means clustering aims to partition n observations into k clusters in
which each observation belongs to the cluster with the nearest mean. Clustering happens
for a certain number of iterations and then clusters converge to their best possible states.
This results in a partitioning of the data space into Voronoi cells.

"GLED K-medoids": Graph Levenshtein Edit Distance K-medoid/K-means clustering
Description: We apply a novel seeding heuristic and a novel clustering distance (GLED) in order
to develop k-medoids clusters of code clone family graphs. The GLED method is described above.
K is not manually set in this method. K is automatically determined by the novel seeding algorithm.

"Iso Kernel": Isomorphic Gaussian Kernel
Description: In machine learning, kernel methods are a class of algorithms for pattern analysis,
whose best known member is the support vector machine (SVM). The general task of pattern analysis
is to find and study general types of relations (for example clusters, rankings, principal components,
correlations, classifications) in data sets. For our tool, we use an isomorphic Gaussian kernel
that compares isomorphic sub-graph patterns of graphs of a certain "k" number of vertices. The
default k is 4 vertices.

"Random Walk Kernel": Random Walk Kernel
Description: A kernel method that compares adjacency label matrices of graphs. Please refer
to our paper "Code Clone Detection and Clustering of Similar Frequent Program Dependence Subgraphs"

The graphs may be non-deterministically handled for finding initial code clone pairs,
but currently we have methods to support either way, as discussed up above.

:author: Shaun Howard
"""


def group_by_families(similar_pairs):
    """
    Groups similar pairs of graphs into clusters
    using the transitive closure of all similar pairs.

    A list of lists of similar subgraph indices (from the graph db)
    is returned.

    :param similar_pairs: the similar pairs of graphs where graphs
    are represented by their graph indices in the graph db
    :return: the clone families, a list of code clones clusters (list of code clone indices)
    """

    # only find the unique pairs in the set and rearrange them with p[0] < p[1]
    clone_family_seeds = sorted(list(set((a, b) if a <= b else (b, a) for a, b in similar_pairs)))

    # a map from clone family seed to set of sub-graphs similar to that seed sub-graph
    clone_families = {}
    orig_num_clones = 0

    # initialize all the sub-graph seed values to a set including the similar sub-graph
    # matched to the sub-graph seed
    for g1, g2 in clone_family_seeds:
        if g1 not in clone_families.keys():
            clone_families[g1] = set()
        clone_families[g1].add(g2)
        orig_num_clones += 1

    # int acting as a boolean, number of duplicates found in closure step
    dupes = 1
    # track the number of duplicate clone families removed from the map
    total_deleted = 0
    # track the number of clone graph numbers skipped because they were already tracked
    total_skipped = 0

    # loop quits when 0 dupes are found
    while dupes:
        # get the sorted clone family seed clones (the initial members of the family buckets)
        sorted_seeds = sorted(clone_families.keys())
        clone_families, ts_step, td_step = eliminate_redundant_clones(sorted_seeds, clone_families)

        # add to the total skipped and total deleted
        total_skipped += ts_step
        total_deleted += td_step

        # remove the empty clone families after removing duplicates
        prev_clone_fam_size = len(clone_families)
        clone_families = remove_empty_families(clone_families)
        dupes = prev_clone_fam_size - len(clone_families)

    clone_family_clusters = []

    # asserts that the clone families found are minimal and transitively closed
    if check_family_closure(clone_families, orig_num_clones, total_deleted, total_skipped):
        # create clone family clusters (lists) of similar sub-graph indices
        clone_family_clusters = create_family_clusters(clone_families)

    return clone_family_clusters


def eliminate_redundant_clones(sorted_seeds, clone_families):
    """
    Eliminates redundant clones for one iteration of transitive
    closure algorithm. Must be run multiple times on the clone
    families to assure all duplicates have been eliminated
    from the clone families list. Will not remove empty families
    as-is. This process must be run after each iteration of duplicates
    are eliminated.
    :param sorted_seeds: the sorted list of family seeds
    to base family organization on
    :param clone_families: the clone families currently w/ possible dups
    :return: the clone families with one layer of duplicates removed,
    possibly all duplicates removed, as well as the total
    clones skipped and total deleted in this elimination step
    """
    # track the number of duplicate clone families removed from the map
    total_deleted = 0
    # track the number of clone graph numbers skipped because they were already tracked
    total_skipped = 0
    # eliminate redundantly-stored clones
    for i in sorted_seeds:
        # do not operation on empty clone families
        if len(clone_families[i]) != 0:
            # run transitive closure on this clone family and every other
            for j in sorted_seeds:
                # assure clone family is not empty
                if len(clone_families[j]) != 0:
                    # run the transitive closure step that groups family j into
                    # family i and empties family j for deletion later
                    clone_families, t_s_step, t_d_step =\
                        transitive_closure_step(clone_families, i, j)
                    total_skipped += t_s_step
                    total_deleted += t_d_step
    return clone_families, total_skipped, total_deleted


def transitive_closure_step(clone_families, i, j):
    """
    Transitive closure step that groups family j into
    family i in the clone families dict and empties
    family j for deletion later.
    :param clone_families: the current clone family dict
    :param i: the clone family to group into
    :param j: the clone family to group
    :return: a step of the transitive closure of the clone families
    as well as the total clones skipped and the total deleted in
    this step
    """
    total_skipped = 0
    total_deleted = 0
    # make sure family i comes before family j
    # and that the intersection of i and j is non-empty
    if i < j and (j in clone_families[i] or
                  len(clone_families[i] & clone_families[j]) > 0):
        for clone in clone_families[j]:
            # accumulate the clones that were not found in family i already
            if clone not in clone_families[i]:
                total_skipped += 1
            # add the clone to family i provided it belongs in i
            clone_families[i].add(clone)
            # accumulate total families deleted from provided family list
            total_deleted += 1
        # clear the old list so it can be eliminated later
        clone_families[j].clear()
    return clone_families, total_skipped, total_deleted


def check_family_closure(clone_families, orig_num_clones, total_deleted, total_skipped):
    """
    Asserts that the number of clones found represents that of the transitive closure of the
    families. Uses the original number of clones, those deleted in the closure process,
    and the number of those skipped during the closure process.
    :param clone_families: the list of clone families
    :param orig_num_clones: the original number of code clone families originally seeded
    :param total_deleted: the number of clones deleted in closure process
    :param total_skipped: the number of clones skipped in closure process
    :return: whether the closure is correct in size
    """
    clone_family_values = []
    for family in clone_families.keys():
        for clone in clone_families[family]:
            clone_family_values.append(clone)
    return len(clone_family_values) == orig_num_clones - (total_deleted - total_skipped)


def create_family_clusters(clone_families):
    """
    Creates a list of clusters (lists) of similar sub-graphs.
    :param clone_families: the list of code clone families
    :return: list of lists of similar families of sub-graphs
    """
    clone_family_clusters = [[key] for key in clone_families.keys()]
    curr_key = 0
    for key in clone_families.keys():
        for clone in clone_families[key]:
            clone_family_clusters[curr_key].append(clone)
        curr_key += 1
    return clone_family_clusters


def remove_empty_families(clone_families):
    """
    Removes the empty lists from the provided
    list of clone families.
    :param clone_families: the list of clone families (lists)
    :return: a list of clone families with all empty lists removed
    """
    families_to_remove = []
    for clone in clone_families.keys():
        if len(clone_families[clone]) == 0:
            families_to_remove.append(clone)

    for clone in families_to_remove:
        del clone_families[clone]

    return clone_families


def find_clone_families(graph_db, metric_type=sim.LED, is_timed=False):
    """
    Searches for and finds code clone families from the given
    graph database (list of lists of triples).
    :param graph_db: the list of graphs where a
    graph is a list of triples.
    :param metric_type: the above listed method choice
    for comparing sub-graphs to determine code clone families,
    which are clusters of similar frequent subgraphs.
    :param is_timed: whether the clone detection algorithm
    should be timed
    :return: the code clone families
    """
    start_time = 0.0
    total_run_time = 0.0
    if is_timed:
        start_time = time.clock()

    # gathers code clone clusters based on a given similarity metric and its parameters (if any)
    clone_families = run_clone_detection(graph_db, metric_type)

    if is_timed:
        total_run_time = time.clock() - start_time
    return clone_families, total_run_time


def run_clone_detection(graph_db, similarity_strategy=sim.LED):
    """
    Runs code clone clustering algorithms. Returns the produced
    code clone families (clusters of similar subgraph indices according
    to the ordering of the graphs in the file index structure).
    :param graph_db: the graph database
    :param similarity_strategy: the strategy to use in similarity detection
    :return: a list of the code clone clusters (lists) of similar sub-graphs indices
    """
    strategy_context = sim.generate_strategy_context(similarity_strategy, graph_db)

    clone_families = None

    if similarity_strategy != sim.KMEANS and similarity_strategy != sim.KMEDOIDS:
        # if not using k-means, find all similar pairs of sub-graphs and create clone families
        # using the transitive closure of all pairs in order to find clone family clusters
        similar_pairs = sim.graph_similarity(graph_db, strategy_context)
        clone_families = group_by_families(similar_pairs)
    elif similarity_strategy == sim.KMEANS:
        # find similar sub-graphs using k-means clustering
        clone_families = kmeans.compute_kmeans_clusters(graph_db, strategy_context, plot_clusters=True)
    elif similarity_strategy == sim.KMEDOIDS:
        # use graph levenshtein edit distance k-medoids clustering
        clone_families = kmedoids.compute_kmedoids_clusters(graph_db)

    return clone_families


