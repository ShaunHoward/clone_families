#!/usr/bin/env python

__author__ = 'Shaun Howard'
from clone_detector import clone_detector as detector
from clone_presenter import clone_renderer as presenter
from clone_presenter import clone_presenter
from argparse import ArgumentParser
import os.path
from utilities import graph_utils as g_util
from similarity_strategy.similarity_strategies import ISO_KERNEL, RW_KERNEL, LED, KMEANS, KMEDOIDS
from sys import exit

DIR = os.path.dirname(__file__)
TRAINING = os.path.abspath(os.path.join(DIR, 'training_data'))
GRAPH_DIR = os.path.join(TRAINING, 'jgit')
GRAPH_ARCHIVE = os.path.join(TRAINING, 'jgit.tar.gz')
FAMILY_OUTPUT_DIR = os.path.join(TRAINING, 'jgit_subset_clone_families')


def parse_input_options():
    parser = ArgumentParser()
    parser.add_argument("-a", "--archive", dest="archive", default='', nargs=1,
                        help="Extract the given graph (.tar.gz) archive to the provided graph directory if it"
                        " doesn't exist.")
    parser.add_argument("-gd", "--graph_directory", dest="graph_directory", default='', nargs=1,
                        help="Perform code clone detection on the provided directory of mined frequent sub-graphs.")
    parser.add_argument("-od", "--output_directory", dest="output_directory", default='', nargs=1,
                        help="Render code clone family clusters to the provided directory in an indexed fashion.")
    parser.add_argument("-ss", "--similarity_strategy", dest="sim_strategy", default=LED, nargs=1,
                        help="Choose a similarity strategy of Levenshtein, Isomorphic Kernel, Random-Walk Kernel,"
                        " Kmeans, or Kmedoids.")
    parser.add_argument('-v', '--visual', action='store', default=True, help='Visualize the clusters.')
    parser.add_argument('-t', '--time', action='store', default=True, help='Time the clustering algorithm.')
    args = parser.parse_args()
    return args


def process_clone_detection_commands(commands):
    """
    Run all provided commands using graph comparison strategy specified in commands.
    Commands can include the tar.gz archive to the graph database, the directory for the extracted graph db,
    whether to calculate the computation time, or to render the clone families created into GIF images.
    :param commands: a parsed argument object using the ArgumentParser class
    """
    # parse similarity metric (strategy) as a constant in the clone detector constants field
    similarity_strategy = determine_similarity_strategy(commands.sim_strategy[0])

    print "Building a graph database from the provided directory of GraphViz dot graphs..."
    # build a graph db of the specified type
    graph_db, graph_dir_root = build_graph_db(commands.archive[0], commands.graph_directory[0], similarity_strategy)

    # make sure the graph db is valid or exit with status code 2
    if not validate_graph_db(graph_db):
        exit(2)

    print "Starting code clone detection..."
    print "Applying the similarity strategy, %s, to find code clone family clusters..." % similarity_strategy
    # find the clone family clusters and the time it took
    clone_families, time = detector.find_clone_families(graph_db, similarity_strategy, commands.time)

    # print time and render clones if necessary
    # need to explicitly check these apparently
    if commands.time == True:
        print 'Code clone detection and clustering completed in {} seconds...'.format(time)
    if commands.visual == True:
        print 'Rendering code clone families...'
        # first, render code clone families
        clone_family_file_paths, local_family_dir = presenter.render_clone_families(clone_families, graph_dir_root,
                                                                                    commands.output_directory[0])

        print 'Displaying code clone families...'
        # second, display rendered code clone families
        clone_presenter.display_clones(clone_family_file_paths)

        print 'Rendered code clone families are located in: %s' % local_family_dir

    print 'Code clone detection and clustering complete.'


def validate_graph_db(graph_db):
    return graph_db is not None and len(graph_db) > 1


def determine_similarity_strategy(option):
    """
    Do a fuzzy string comparison to determine which similarity strategy to use.
    The Levenshtein edit distance is the default similarity strategy used.
    :param option: the similarity strategy string option from command line
    :return: the similarity strategy as represented in the program
    """
    strategy = LED
    option = option.lower()
    if option is RW_KERNEL.lower() or ("random" in option and "kernel" in option):
        strategy = RW_KERNEL
    elif option is ISO_KERNEL.lower() or ("iso" in option and "kernel" in option):
        strategy = ISO_KERNEL
    elif option is KMEDOIDS.lower() or ("k" in option and "medoids" in option):
        strategy = KMEDOIDS
    elif option is KMEANS.lower() or ("k" in option and "means" in option):
        strategy = KMEANS

    return strategy


def validate_args(args):
    """
    Considers if the provided arguments are valid given that
    a graph and output directory are provided and either the
    graph directory or .tar.gz archive of graphs exist.
    :param args: an argument parser object intended for
    detecting and clustering code clones.
    :return: whether the provided arguments are valid
    """

    is_valid, gd_exists = check_arg_directory(args.graph_directory)
    is_valid = is_valid and check_rendering_args(args)
    if is_valid and gd_exists:
        return True
    elif is_valid:
        is_valid, arc_exists = check_arg_directory(args.archive)
        if is_valid and arc_exists:
            return True
    return False


def check_rendering_args(args):
    return args.visual and args.output_directory is not None and type(args.output_directory) == list \
        and len(args.output_directory) > 0 and type(args.output_directory[0]) == str


def check_arg_directory(arg_dir):
    """
    Checks that the provided argument directory is valid.
    It is only valid when it is a list containing a string
    with a valid directory.
    :param arg_dir: the list of the argument directory string
    :return: whether the arg directory is a valid directory in valid format
    and whether that directory exists
    """
    return ((arg_dir is not None and type(arg_dir) == list and len(arg_dir) > 0 and type(arg_dir[0]) == str),
            os.path.exists(arg_dir[0]))


def build_graph_db(archive, directory, similarity_metric):
    """
    Build a graph database for comparison under the provided similarity metric.
    Create the database from the specified archive or directory, if the directory exists.
    The graphs in the database may be triples of source, destination and edge labels if
    the Levenshtein similarity metric is used. The graphs will be simple graphs when the
    kernel similarity metric is used.
    :param archive: a tar.gz archive of the graph database
    :param directory: a directory to store or find the graph database
    :param similarity_metric: the metric used to compare all pairs of graphs in the database
    :return: the graph database as a list and the graph directory root
    """
    if similarity_metric == LED:
        graph_db, graph_dir_root = g_util.create_graph_db(archive, directory, make_triples=True)
    else:
        graph_db, graph_dir_root = g_util.create_graph_db(archive, directory, make_triples=False)

    return graph_db, graph_dir_root


if __name__ == '__main__':
    # read input options from command line
    args_ = parse_input_options()

    # validate that proper arguments were provided
    valid_args = validate_args(args_)

    if valid_args:
        # parsed option list is fed as input to be processed
        process_clone_detection_commands(args_)
    else:
        print "Could not process arguments. Please try again."
        exit(1)
