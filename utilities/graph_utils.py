__author__ = 'Shaun Howard'

from dot_tools import parse
from dot_tools.dot_graph import SimpleGraph
import os
import io_utils as util
import numpy as np
from matplotlib import pyplot
from Levenshtein import hamming


def show_clusters(clusters, bsde_codes, type):
    """
    Plots the clusters according to the hamming distance of
    their bsde codes from a string of all periods like the k-medoids seeding algorithm uses.
    :param clusters: the resultant clusters from clustering
    :param bsde_codes: the list of bsde codes used for clustering
    """
    xs = []
    ys = []
    for curr_clust in range(len(clusters)):
        cluster = clusters[curr_clust]

        for data_point in cluster:
            x = curr_clust
            xs.append(x)
            code = bsde_codes[data_point]
            y = hamming(code, '.'*len(code))
            ys.append(y)
    fig, ax = pyplot.subplots()
    ax.scatter(xs[:], ys[:], alpha=0.5)
    ax.set_xlabel('Cluster', fontsize=20)
    ax.set_ylabel('Distance from Period String', fontsize=20)
    ax.set_title(type + ' Cluster Similarity')

    ax.grid(True)
    fig.tight_layout()
    pyplot.show()


def create_graph_db(graph_archive, graph_dir, make_triples=True):
    """
    Creates a graph database given the graph directory.
    The graph db can be a list of lists of triples
    where (Source Node Label, Dest. Node Label, Edge Label) represents
    the structure of each triple and a list in a row makes up one graph.
    The graph db could also be a list of SimpleGraphs from dot tools package.
    :param graph_archive: the path to the graph archive to extract if the given
    graph directory does not already exist
    :param graph_dir: the path to the graph directory that either may exist and
    contain graphs or will be created and populated with graphs by the provided
    graph archive
    :param make_triples: whether to make a graph db of triples or simple graphs
    :return: the graph db, a list of either triples or simple graphs,
    and the graph directory root
    """
    # un-archive the graph files to the local graph directory
    if not os.path.exists(graph_dir):
        print "Un-extracting the graph archive and using that for code clone detection..."
        util.untar_file(graph_archive, graph_dir)
    else:
        print "Using the existing graph directory for code clone detection..."
    # create the graph database
    graph_db, graph_dir_root = read_pattern_graphs(graph_dir, make_triples)
    return graph_db, graph_dir_root


def viz2simple_graph(graph_string):
    """
    Builds a simple graph from the given GraphViz
    .dot graph string.
    :param graph_string: a graph viz .dot graph string
    :return: a simple graph from the graph string
    """
    tree = parse(graph_string)
    g = SimpleGraph.build(tree.kid('Graph'))
    return g


def extract_triples_from_graph(graph):
    """
    Given the input graph, this function finds all of the
    source->destination node pairs as well as the edges between those
    and stores their labels in a list. Hence, for each path between
    a source node and destination node in the given graph, it will
    store the label of the source node, the label of the destination node,
    and the label of the edge between those nodes in a triple. A graph
    thus represents a list of triples in this case.

    Ordering of output is from the Bliss algorithm. Triples
    represent the canonical labeling of the input simple graph.

    :param graph: a simple graph
    :return: a list of triples that represent the simple graph
    """
    triple_list = []
    for edge in graph.edges:
        # 0 is source node label
        # 1 is destination node label
        # 2 is the edge label from source to dest
        triple = (graph.nodes[edge[0]]['label'], graph.nodes[edge[1]]['label'], edge[2])
        triple_list.append(triple)
    return triple_list


def read_pattern_graphs(graph_dir, make_triples=True):
    """
    Reads and parses all of the "pattern.dot" graphs in the given directory.
    Returns a list of graphs in triple form.
    """

    graph_file_paths, root = get_graph_file_paths(graph_dir, 'pattern.dot')

    graphs_ = []
    if graph_file_paths is not None and len(graph_file_paths) > 1:
        for graph_file_path in graph_file_paths:
            # first, read the string of the graph
            graph_string = util.read_file_contents(graph_file_path)

            # second, parse the string into a simple graph form
            simple_graph = parse_simple_graph(graph_string)

            # either make the graph into triples or an adjacency list
            # ordering of triples is produced by the Bliss algorithm
            # triples represent canonical labeling of graphs
            if make_triples:
                graph = triples(simple_graph)
            else:
                # keep the simple graph representation and use the edges and nodes lists
                graph = simple_graph
            graphs_.append(graph)

    return graphs_, root


def build_graph_hash_table(simple_graph):
    """
    Builds a hash table from the provided simple graph
    where the key is a source label and the value a dictionary
    with its key as the edge label and its value as a list of
    destination labels along the path. Empty edge labels
    are referred to as 'empty_edge' in the table.
    :param simple_graph: the graph to build a hash table from
    :return: a hash table from the provided sub-graph
    """

    graph_table = dict()
    for edge in simple_graph.edges:
        source_label = simple_graph.nodes[edge[0]]['label']
        if source_label not in graph_table:
            graph_table[source_label] = dict()
        edge_label = edge[2] if edge[2] else 'empty_edge'
        if edge_label not in graph_table[source_label]:
            graph_table[source_label][edge_label] = set()
        dest_label = simple_graph.nodes[edge[1]]['label']
        graph_table[source_label][edge_label].add(dest_label)

    return graph_table


def get_pattern_graph_path(pattern_number, local_graph_dir):
    """
    Gets the file path of the pattern dot graph for the given pattern number.
    :param pattern_number: the number of the pattern to get the pattern graph of
    :param local_graph_dir: the parent directory of the graph data set
    :return: the file path for the pattern dot graph file of the pattern number provided
    """
    return '/'.join([local_graph_dir, str(pattern_number), 'pattern.dot'])


def get_embedded_graph_paths(pattern_number, local_graph_dir):
    """
    Gets the file paths of all the embedded dot graphs for the given pattern number.
    :param pattern_number: the number of the pattern to get the embedded graphs of
    :param local_graph_dir: the parent directory of the graph data set
    :return: a list of the embedded graph file paths for the provided pattern number
    """
    instance_graph_dir = '/'.join([local_graph_dir, str(pattern_number), 'instances'])
    embedded_paths, root = get_graph_file_paths(instance_graph_dir, 'embedding.dot')
    return embedded_paths


def parse_simple_graph(graph_string):
    """
    Parses the contents of a graph viz .dot into a string
    and then transforms that into a simple graph.
    :param graph_string: a graph viz dot graph string
    :return: a simple graph with a list of nodes and edges
    """

    return viz2simple_graph(graph_string)


def triples(simple_graph):
    """
    Parses the contents of a graph viz string
    then turns it into a matrix representation of triples.

    The ordering of triples is produced by the Bliss algorithm.
    The triples represent the canonical labeling of graphs.

    Each triple has the following:
    [0]: Source node label
    [1]: Destination node label
    [2]: Edge label
    """
    triples_ = extract_triples_from_graph(simple_graph)
    return triples_


def get_graph_file_paths(dir_, file_name):
    """
    Returns all of the graph files with the specified file name in the provided directory.
    """

    paths_in_dir, root = util.get_dot_file_paths(dir_, file_name)
    return paths_in_dir, root


def get_adjacency_matrix(nodes, edges):
    """
    Creates an adjacency matrix from the provided node dict and
    edge list.
    :param nodes: a dictionary of nodes
    :param edges: a list of edges (as tuples)
    :return: a numpy adjacency matrix for the graph nodes and edges
    """
    adjacency = np.zeros((len(nodes), len(nodes)), dtype=np.int)
    node_keys = nodes.keys()
    for e in edges:
        adjacency[node_keys.index(e[0])][node_keys.index(e[1])] = 1
    return adjacency


def get_adjacency_matrix_db(graph_db):
    """
    Converts all simple graphs in the graph database
    to adjacency matrices and returns them as a list.
    :param graph_db: the database of simple graphs
    :return: a list of adjacency matrices
    """
    return [get_adjacency_matrix(graph.nodes, graph.edges) for graph in graph_db]


def graph_2_string(graph):
    """
    Converts a graph the string bsde code of its graph.
    :param graph: the graph to convert to string
    :return: the bsde code for the graph
    """
    return triples_2_string(triples(graph))


def triples_2_string(triples):
    """
    Converts a triple bsde graph representation into its bsde code.
    :param triples: the list of graph triples to convert to a string
    :return: the graph as a unique bsde code
    """

    g_joins = []

    # make the triples for each graph into strings
    for g in triples:
        g_joins.append(''.join(g))

    # sort graphs by tuples to make sure comparison is consistent
    g_joins = sorted(g_joins)

    # join all of the sorted triples into strings, each string representing
    # each graph
    g_joined = ''.join(g_joins)

    return g_joined


def normalize_list(scores):
    min_val = min(scores)
    max_val = max(scores)
    # attempt to normalize the scores produced by the comparison method
    if max_val - min_val != 0:
        for i in range(len(scores)):
            scores[i] = (scores[i]-min_val)/(max_val-min_val)
    return scores


def normalize(scores, column_wise=False):
    """
    Normalizes the score matrix to range [0, 1] using its min and max values.
    :param scores: the matrix of scores to normalize
    :param column_wise: whether or not to normalize data by column or entire patterns
    :return: the normalized score matrix
    """
    if column_wise:
        for j in range(scores.shape[1]):
            min_val = np.min(scores[:, j])
            max_val = np.max(scores[:, j])
            # attempt to normalize the scores produced by the comparison method
            if max_val - min_val != 0:
                scores[:, j] = (scores[:, j]-min_val)/(max_val-min_val)
    else:
        min_val = scores.min()
        max_val = scores.max()
        # attempt to normalize the scores produced by the comparison method
        if max_val - min_val != 0:
            for i in range(len(scores)):
                for j in range(len(scores[i])):
                    scores[i][j] = (scores[i][j]-min_val)/(max_val-min_val)

    return scores