__author__ = 'Shaun Howard'

import os
from os.path import expanduser
import shutil


def append_path_to_home(file_path):
    return file_path


def untar_file(tar_path, output_path):
    """
    Un-archives a tar file to the given output directory.
    """

    import tarfile
    tar = tarfile.open(tar_path)

    tar.extractall(output_path)
    tar.close()


def read_file_contents(file_path):
    f = open(file_path, 'r')
    content_list = []
    for line in f:
        content_list.append(line)
    return '\n'.join(content_list)


def get_dot_file_paths(directory, file_name):
    """
    Finds the files with 'file_name' in the given indexed directory
    structure for subgraphs.
    """

    file_paths = []
    sub_dirs = []
    root = ""
    for root, dirs, files in os.walk(directory):
        for file_ in files:
            if file_.endswith(file_name):
                sub_dirs.append(root)
    sub_dir_names = []
    if sub_dirs is not None and len(sub_dirs) > 0:
        for sub_dir in sub_dirs:
            sub_dir_names.append(os.path.basename(os.path.normpath(sub_dir)))
        sub_dir_nums = []
        try:
            root = os.path.dirname(sub_dirs[0])
            # order subdirs to read graphs in order
            sub_dir_nums = [int(x) for x in sub_dir_names]
            sub_dir_nums.sort()
            sub_dir_nums = [str(x) for x in sub_dir_nums]
        except:
            pass

        if root and len(sub_dir_nums) > 1:
            for sub_dir_num in sub_dir_nums:
                file_paths.append('/'.join([root, sub_dir_num, file_name]))
        else:
            print "Cannot find root graph directory. Quitting..."
    else:
        print "Cannot find graph structure. Quitting..."
    return file_paths, root


def create(dir):
    """
    Creates the given directory if it does not exist.
    :param dir: the dir to create
    """
    if not os.path.exists(dir):
        os.makedirs(dir)


def delete_if_exists(dir):
    """
    Deletes the given directory if it exists.
    :param dir: the dir to delete
    """
    if os.path.exists(dir):
        shutil.rmtree(dir)
