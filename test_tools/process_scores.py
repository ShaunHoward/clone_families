import os
import test_tools_utils as ttu
from similarity_strategy.similarity_strategies import LED, ISO_KERNEL, RW_KERNEL


def parse_process_scores_summary(f):
    led = 0
    iso = 1
    rw = 2
    ansg = [[], [], []]
    thresh = [[], [], []]

    for line in f.readlines():
        if len(line) == 0 or line[0] == '\n':
            continue

        values = line.split('\t')

        ansg[led].append(float(values[1]))
        ansg[iso].append(float(values[2]))
        ansg[rw].append(float(values[3]))

        thresh[led].append(float(values[4]))
        thresh[iso].append(float(values[5]))
        if values[6].endswith('\n'):
            thresh[rw].append(float(values[6][:len(values[6])-1]))
        else:
            thresh[rw].append(float(values[6]))

    return ansg, thresh


# two modes of operation:
# 1. find the optimal threshold value to generate lists of similar graphs of a specified average length
# 2. using a specified threshold value, generate lists of similar graphs

#NOTE (9-12-2015): Hey, Rebecca! There's this thing called P-I-D control
# that I know you've known about for a very long time, and which you should probably use
# instead of the current almost-not-quite approximation to P-I-D...
# Also, consider handling >2-cycles
def find_threshold_for_avg(scores, avg, start_thresh, delta_avg):
    similar_graphs, temp_avg = ttu.get_similar_graphs(scores, start_thresh)
    if abs(temp_avg - avg) <= delta_avg:
        return similar_graphs, temp_avg, start_thresh
    threshold = start_thresh
    start_diff = abs(temp_avg - avg)
    last_sim_graphs = similar_graphs
    last_temp_avg = temp_avg
    last_thresh = threshold
    last_diff = abs(temp_avg - avg)
    delta = 0.001
    max_iter = 50
    it = 0
    while abs(temp_avg - avg) > delta_avg and it < max_iter:
        if temp_avg > avg:
            if last_temp_avg < avg and last_diff < abs(temp_avg - avg):
                return last_sim_graphs, last_temp_avg, last_thresh
            last_thresh = threshold
            threshold += (delta * (1 + (temp_avg - avg)/float(start_diff)))
        else:
            if last_temp_avg > avg and last_diff < abs(temp_avg - avg):
                return last_sim_graphs, last_temp_avg, last_thresh
            last_thresh = threshold
            threshold -= (delta * (1 + (temp_avg - avg)/float(start_diff)))
        it += 1
        last_temp_avg = temp_avg
        last_diff = abs(temp_avg - avg)
        last_sim_graphs = similar_graphs
        similar_graphs, temp_avg = ttu.get_similar_graphs(scores, threshold)
        print 'iter', it, ': ansg=', temp_avg, 'thresh=', threshold
    return similar_graphs, temp_avg, threshold

def print_similar_graphs_to_file(similar_graphs, scores, f):
    for i in range(len(similar_graphs)):
        for graph in similar_graphs[i]:
            f.write(str(graph) + ':' + str(scores[i][graph]) + ' ')
        f.write('\n')


def process_scores(flag_find_thresh, scores, tansg, threshold, eps_tansg, output_file, summary_file):
    new_threshold = threshold
    if flag_find_thresh:
        sim_graphs, ansg, new_threshold = find_threshold_for_avg(scores, tansg, threshold, eps_tansg)
        print 'tansg:', tansg
        print 'ansg:', ansg
        print 'threshold:', new_threshold
    else:
        sim_graphs, ansg = ttu.get_similar_graphs(scores, threshold)
        print 'average number of similar graphs for threshold', threshold, ': ', ansg
    print_similar_graphs_to_file(sim_graphs, scores, output_file)
    summary_file.write('tansg: ' + str(tansg) + '\n')
    summary_file.write('ansg: ' + str(ansg) + '\n')
    summary_file.write('epsilon for tansg: ' + str(eps_tansg) + '\n')
    summary_file.write('threshold: ' + str(new_threshold) + '\n')


def main(flag_find_thresh, target_ansg, threshold_led=0.9, threshold_iso=0.9, threshold_rw=0.9, eps_tansg_led=0.005, eps_tansg_iso=0.005, eps_tansg_rw=0.005, outdir=None):
    led_scores = ttu.parse_scores(ttu.get_default_score_file(LED))
    iso_scores = ttu.parse_scores(ttu.get_default_score_file(ISO_KERNEL))
    rw_scores = ttu.parse_scores(ttu.get_default_score_file(RW_KERNEL))

    if outdir == None:
        to_append = '/results/dat/tansg/' + str(target_ansg)
        if os.getcwd().endswith('test_tools'):
            outdir = os.getcwd() + to_append
        else:
            outdir = os.getcwd() + '/test_tools' + to_append
    else:
        outdir = os.getcwd() + outdir
    if not os.path.exists(outdir):
        os.mkdir(outdir)

    led_output_file = outdir + '/LED_SIM_GRAPHS.dat'
    iso_output_file = outdir + '/ISO_SIM_GRAPHS.dat'
    rw_output_file = outdir + '/RW_SIM_GRAPHS.dat'
    summary_file = outdir + '/summary.dat'
    f_led = open(led_output_file, 'w')
    f_iso = open(iso_output_file, 'w')
    f_rw = open(rw_output_file, 'w')
    f_summary = open(summary_file, 'w')

    if flag_find_thresh:
        f_summary.write('finding optimal threshold value to generate a given average number of similar graphs.\n')
        f_summary.write('starting threshold for led: ' + str(threshold_led) + '\n')
        f_summary.write('starting threshold for iso: ' + str(threshold_iso) + '\n')
        f_summary.write('starting threshold for rw: ' + str(threshold_rw) + '\n')
    else:
        f_summary.write('generating lists of similar graphs for a specified threshold value.\n')

    f_summary.write('\nLED:\n')
    process_scores(flag_find_thresh, led_scores, target_ansg, threshold_led, eps_tansg_led, f_led, f_summary)

    f_summary.write('\nISO KERNEL:\n')
    process_scores(flag_find_thresh, iso_scores, target_ansg, threshold_iso, eps_tansg_iso, f_iso, f_summary)

    f_summary.write('\nRW KERNEL:\n')
    process_scores(flag_find_thresh, rw_scores, target_ansg, threshold_rw, eps_tansg_rw, f_rw, f_summary)

    f_led.close()
    f_iso.close()
    f_rw.close()
    f_summary.close()


if __name__ == '__main__':
    main(True, 8, threshold_led=0.805, threshold_iso=0.99, threshold_rw=0.64)
