import os
import numpy as np
from matplotlib import pyplot as plt
from compare_scores import parse_compare_scores_summary


def main():
    to_append = '/results/dat/tansg'
    if os.getcwd().endswith('test_tools'):
        summary_dir = os.getcwd() + to_append
    else:
        summary_dir = os.getcwd() + '/test_tools' + to_append

    tansg, iso_rw, iso_led, rw_led = parse_compare_scores_summary()
    tansg_x = np.array(tansg)

    plt.clf()
    plt.scatter(tansg_x, np.array(iso_rw), c='r')
    plt.scatter(tansg_x, np.array(iso_led), c='g')
    plt.scatter(tansg_x, np.array(rw_led), c='b')
    plt.xlabel('TANSG')
    plt.ylabel('Number of Common Similar Graphs')
    plt.xlim(0, 21)
    plt.ylim(0, 7)
    plt.suptitle('Number of Common Similar Graphs vs TANSG\nfor iso-rw (red), iso-led (green), and rw-led (blue)\n')
    plt.savefig(summary_dir + '/scatter_compare_num_sim_graphs.png')


if __name__ == '__main__':
    main()