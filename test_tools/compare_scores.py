from similarity_strategy.similarity_strategies import LED, ISO_KERNEL, RW_KERNEL
import test_tools_utils as ttu
from process_scores import parse_process_scores_summary
import os


def parse_compare_scores_com_sim_graphs(filename):
    f = open(filename, 'r')
    lines = f.readlines()
    csg_line = lines[len(lines)-1]
    csg = csg_line.split(':')[1]
    csg.strip(' ')
    csg.strip('\n')
    csg = float(csg)
    print csg_line
    print 'num common similar graphs:', csg
    f.close()
    return csg


def parse_compare_scores_summary():
    tansg_dirs = get_tansg_dirs()
    tansg = []
    iso_rw = []
    iso_led = []
    rw_led = []
    for dir in tansg_dirs:
        print 'reading from ', dir + '/com_sim_graphs_iso_led.dat'
        iso_led.append(parse_compare_scores_com_sim_graphs(dir + '/compare_iso_led.dat'))
        iso_rw.append(parse_compare_scores_com_sim_graphs(dir + '/compare_iso_rw.dat'))
        rw_led.append(parse_compare_scores_com_sim_graphs(dir + '/compare_rw_led.dat'))
        tansg.append(float(os.path.split(dir)[1]))
    return tansg, iso_rw, iso_led, rw_led

def correlate_labels(common_similar_graphs, simple_graph_db):
    avg_node_diffs, avg_edge_diffs, avg_sim_graphs, sim_graph_count = 0, 0, 0, 0
    for graph_list in common_similar_graphs:
        avg_sim_graphs += len(graph_list)
        for index1 in graph_list:
            for index2 in graph_list:
                if not index1 == index2: 
                    sg1 = simple_graph_db[index1]
                    sg2 = simple_graph_db[index2]
                    sg1_node_labels = [value['label'] for value in sg1.nodes.values()]
                    sg2_node_labels = [value['label'] for value in sg2.nodes.values()]
                    sg1_edge_labels = [edge[2] for edge in sg1.edges]
                    sg2_edge_labels = [edge[2] for edge in sg2.edges]
                    avg_node_diffs += len(set(sg1_node_labels).symmetric_difference(set(sg2_node_labels)))
                    avg_edge_diffs += len(set(sg1_edge_labels).symmetric_difference(set(sg2_edge_labels)))
                    sim_graph_count += 1
    if sim_graph_count == 0:
        avg_node_diffs = 0
        avg_edge_diffs = 0
    else:
        avg_node_diffs = float(avg_node_diffs)/sim_graph_count
        avg_edge_diffs = float(avg_edge_diffs)/sim_graph_count
    if len(common_similar_graphs) == 0:
        avg_sim_graphs = 0
    else:
        avg_sim_graphs = float(avg_sim_graphs)/len(common_similar_graphs)
    return avg_node_diffs, avg_edge_diffs, avg_sim_graphs

def print_common_similar_graphs_to_file(common_similar_graphs, scores1, scores2, f):
    for i in range(len(common_similar_graphs)):
        for index in common_similar_graphs[i]:
            f.write(str(index) + ':' + str(scores1[i][index]) + ' ')
        f.write('\n')
        for index in common_similar_graphs[i]:
            f.write(str(index) + ':' + str(scores2[i][index]) + ' ')
        f.write('\n\n')


def compare_scores_with_thresholds(simple_graph_db, scores1, scores2, thresh1, thresh2, f_summary, f_sim_graphs=None):
    sim_graphs1, avg1 = ttu.get_similar_graphs(scores1, thresh1)
    sim_graphs2, avg2 = ttu.get_similar_graphs(scores2, thresh2)

    common_similar_graphs = ttu.get_common_similar_graphs(sim_graphs1, sim_graphs2)

    if not f_sim_graphs == None:
        print_common_similar_graphs_to_file(common_similar_graphs, scores1, scores2, f_sim_graphs)

    avg_node_diffs, avg_edge_diffs, avg_sim_graphs = correlate_labels(common_similar_graphs, simple_graph_db)

    print 'average number of node label diffs:', avg_node_diffs
    print 'average number of edge label diffs:', avg_edge_diffs
    print 'average number of common similar graphs:', avg_sim_graphs

    f_summary.write('avg node label diffs: ' + str(avg_node_diffs) + '\n')
    f_summary.write('avg edge label diffs: ' + str(avg_edge_diffs) + '\n')
    f_summary.write('avg # common similar graphs: ' + str(avg_sim_graphs) + '\n')


def get_default_tansg_dir():
    to_append = '/results/dat/tansg'
    if os.getcwd().endswith('test_tools'):
        tansg_base_dir = os.getcwd() + to_append
    else:
        tansg_base_dir = os.getcwd() + '/test_tools' + to_append
    if not os.path.exists(tansg_base_dir):
        os.mkdir(tansg_base_dir)
    return tansg_base_dir


def get_tansg_dirs(tansg_base_dir=None):
    if tansg_base_dir == None:
        tansg_base_dir = get_default_tansg_dir()
    tansg_dirs = os.popen('find ' + tansg_base_dir + '/*' + ' -maxdepth 0 -type d').readlines()
    for i in range(len(tansg_dirs)):
        tansg_dirs[i] = tansg_dirs[i].strip('\n')
    return tansg_dirs


def main():
    simple_graph_db = ttu.get_simple_graph_db()

    led_scores = ttu.parse_scores(ttu.get_default_score_file(LED))
    iso_scores = ttu.parse_scores(ttu.get_default_score_file(ISO_KERNEL))
    rw_scores = ttu.parse_scores(ttu.get_default_score_file(RW_KERNEL))

    tansg_base_dir = get_default_tansg_dir()

    tansg_dirs = get_tansg_dirs()

    score_summary = tansg_base_dir + '/summary.dat'
    f_score_summary = open(score_summary, 'r')

    ansg, thresh = parse_process_scores_summary(f_score_summary)
    f_score_summary.close()

    led = 0
    iso = 1
    rw = 2

    for i in range(len(ansg[0])):
        iso_thresh = thresh[iso][i]
        rw_thresh = thresh[rw][i]
        led_thresh = thresh[led][i]

        d = tansg_dirs[i]

        f_summary_iso_rw = open(d + '/compare_iso_rw.dat', 'w')
        f_summary_iso_led = open(d + '/compare_iso_led.dat', 'w')
        f_summary_rw_led = open(d + '/compare_rw_led.dat', 'w')

        f_sim_graphs_iso_rw = open(d + '/com_sim_graphs_iso_rw.dat', 'w')
        f_sim_graphs_iso_led = open(d + '/com_sim_graphs_iso_led.dat', 'w')
        f_sim_graphs_rw_led = open(d + '/com_sim_graphs_rw_led.dat', 'w')

        compare_scores_with_thresholds(simple_graph_db, iso_scores, rw_scores, iso_thresh, rw_thresh, f_summary_iso_rw, f_sim_graphs_iso_rw)
        compare_scores_with_thresholds(simple_graph_db, iso_scores, led_scores, iso_thresh, led_thresh, f_summary_iso_led, f_sim_graphs_iso_led)
        compare_scores_with_thresholds(simple_graph_db, rw_scores, led_scores, rw_thresh, led_thresh, f_summary_rw_led, f_sim_graphs_rw_led)

        f_summary_iso_rw.close()
        f_summary_iso_led.close()
        f_summary_rw_led.close()
        f_sim_graphs_iso_rw.close()
        f_sim_graphs_iso_led.close()
        f_sim_graphs_rw_led.close()

if __name__ == '__main__':
    main()
