from test_tools import test_tools_utils as ttu
from similarity_strategy.similarity_strategies import LED, ISO_KERNEL, RW_KERNEL


def compute_avg_score(scores):
    avg = 0.0
    for score_list in scores:
        for score in score_list:
            avg += score
    return avg / float(len(scores)*len(scores))


def avg_score(sim_method):
    scores = ttu.parse_scores(ttu.get_default_score_file(sim_method))
    return compute_avg_score(scores)


def summarize_avg_scores():
    score_dir = ttu.get_default_score_dir()
    summary_file = open(score_dir + '/avg_scores.dat', 'w')

    avg = avg_score(LED)
    summary_file.write('LED avg score: ' + str(avg) + '\n')

    avg = avg_score(ISO_KERNEL)
    summary_file.write('ISO avg score: ' + str(avg) + '\n')

    avg = avg_score(RW_KERNEL)
    summary_file.write('RW avg score: ' + str(avg) + '\n')

    summary_file.close()


def main():
    #generate score files for the 3 methods
    print 'generating GLED scores...'
    ttu.generate_scores_file(LED)
    print 'generating ISO KERNEL scores...'
    ttu.generate_scores_file(ISO_KERNEL)
    print 'generating RW KERNEL scores...'
    ttu.generate_scores_file(RW_KERNEL)

    summarize_avg_scores()


if __name__ == '__main__':
    main()