import os
import numpy as np
from matplotlib import pyplot as plt
from process_scores import parse_process_scores_summary


def main():
    to_append = '/results/dat/tansg'
    if os.getcwd().endswith('test_tools'):
        summary_dir = os.getcwd() + to_append
    else:
        summary_dir = os.getcwd() + '/test_tools' + to_append
    f = open(summary_dir + '/summary.dat', 'r')

    led = 0
    iso = 1
    rw = 2

    ansg, thresh = parse_process_scores_summary(f)

    f.close()

    plt.clf()
    plt.scatter(np.array(ansg[led]), np.array(thresh[led]),c='r')
    plt.scatter(np.array(ansg[iso]), np.array(thresh[iso]), c='g')
    plt.scatter(np.array(ansg[rw]), np.array(thresh[rw]), c='b')
    plt.xlabel('ANSG')
    plt.ylabel('threshold value')
    plt.suptitle('Average Number of Similar Graphs vs. Threshold Value\nfor LED (red), ISO (green), and RW (blue)\n',)
    plt.xlim(0,21)
    plt.ylim(0.5,1.1)
    plt.savefig(summary_dir + '/scatter_process_ansg.png')


if __name__ == '__main__':
    main()