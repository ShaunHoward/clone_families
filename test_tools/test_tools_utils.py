import os

from similarity_strategy import similarity_strategies as sim
from similarity_strategy.similarity_strategies import LED, ISO_KERNEL, RW_KERNEL
from utilities import graph_utils as gu
import clone_commander as cc

SCORE_DIR = '/projects/eecs493/clone_families/test_tools/results/dat/scores'
GRAPH_DIR = '/projects/eecs493/clone_families/training_data/jgit_subset'
GRAPH_ARCHIVE = '/projects/eecs493/clone_families/training_data/jgit.tar.gz'

def get_graph_db(sim_method):
    graph_db = cc.build_graph_db(GRAPH_ARCHIVE, GRAPH_DIR, sim_method)
    return graph_db[0]


def get_triple_graph_db():
    graph_db = gu.create_graph_db(GRAPH_ARCHIVE, GRAPH_DIR, make_triples=True)
    return graph_db[0]


def get_simple_graph_db():
    graph_db = gu.create_graph_db(GRAPH_ARCHIVE, GRAPH_DIR, make_triples=False)
    return graph_db[0]


def get_default_score_dir():
    score_dir = os.path.expanduser('~') + SCORE_DIR
    if not os.path.exists(score_dir):
        os.mkdir(score_dir)
    return score_dir


def get_default_score_file(sim_method):
    score_file = get_default_score_dir()
    if sim_method == LED:
        score_file += '/LED'
    elif sim_method == ISO_KERNEL:
        score_file += '/ISO_KERNEL'
    elif sim_method == RW_KERNEL:
        score_file += '/RW_KERNEL'
    else:
        return
    score_file += '_SCORES.dat'
    return score_file


def generate_scores_file(sim_method):
    graph_db = get_graph_db(sim_method)

    scores = sim.generate_similarity_scores(graph_db, sim.generate_strategy_context(sim_method, graph_db))

    score_file = get_default_score_file(sim_method)

    f = open(score_file, 'w')

    for i in range(scores.shape[0]): #rows
        for j in range(scores.shape[1]): #cols
            f.write(str(scores[i,j]) + ' ')
        f.write('\n')

    f.close()


def parse_scores(filename):
    f = open(filename, 'r')
    scores = []
    for line in f.readlines():
        if len(line) > 0 and not line[0] == '\n':
            linescores = []
            for score in line.split(' '):
                if not score[0] == '\n':
                    linescores.append(float(score))
            scores.append(linescores)
    f.close()
    return scores


def get_similar_graphs(scores, threshold):
    similar_graphs = [[] for i in range(len(scores))]
    avg = 0
    for g in range(len(scores)):
        for gi in range(len(scores[g])):
            if scores[g][gi] >= threshold:
                similar_graphs[g].append(gi)
        avg += len(similar_graphs[g])
    avg = float(avg) / len(similar_graphs)
    return similar_graphs, avg


def get_common_similar_graphs(sim_graphs1, sim_graphs2):
    common_similar_graphs = []
    for i in range(len(sim_graphs1)):
        common_graphs = set(sim_graphs1[i]).intersection(set(sim_graphs2[i]))
        common_similar_graphs.append(common_graphs)
    return common_similar_graphs

