from test_tools import test_tools_utils as ttu
from similarity_strategy import similarity_strategies as sim
from similarity_strategy.similarity_strategies import LED, ISO_KERNEL, RW_KERNEL
import math
import os


def compare_methods(scores1, scores2):
    p = 0.0
    ngraphs = len(scores1)
    for i in range(ngraphs):
        for j in range(ngraphs):
            if scores1[i][j] > scores2[i][j]:
                p += 1.0
            elif scores1[i][j] == scores2[i][j]:
                p += 0.5
    return p / math.pow(ngraphs, 2)


def main():
    iso_scores = ttu.parse_scores(ttu.get_default_score_file(ISO_KERNEL))
    rw_scores = ttu.parse_scores(ttu.get_default_score_file(RW_KERNEL))
    led_scores = ttu.parse_scores(ttu.get_default_score_file(LED))

    p_iso_rw = compare_methods(iso_scores, rw_scores)
    p_iso_led = compare_methods(iso_scores, led_scores)
    p_rw_led = compare_methods(rw_scores, led_scores)

    print 'probability that iso kernel classifies two graphs as more similar than rw kernel:', p_iso_rw
    print 'probability that iso kernel classifies two graphs as more similar than led kernel:', p_iso_led
    print 'probability that rw kernel classifies two graphs as more similar than led kernel:', p_rw_led

    f = open(ttu.get_default_score_dir() + '/roc_summary.dat', 'w')
    f.write('probability that iso kernel classifies two graphs as more similar than rw kernel: ' + str(p_iso_rw) + '\n')
    f.write('probability that iso kernel classifies two graphs as more similar than led kernel: ' + str(p_iso_led) + '\n')
    f.write('probability that rw kernel classifies two graphs as more similar than led kernel: ' + str(p_rw_led) + '\n')
    f.close()

if __name__ == '__main__':
    main()
