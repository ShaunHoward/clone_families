#!/usr/bin/env python

import sys

from clone_frame import CloneFrame
from os import listdir
from os.path import isdir
from Tkinter import Tk, N, E, S, W


class CloneGUI(Tk):

    def __init__(self, paths):
        Tk.__init__(self)
        self.paths = paths
        self.title('Clone Viewer')

        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)

        left_frame = CloneFrame(master=self)
        left_frame.grid(row=0, column=0, sticky=N+E+S+W)
        right_frame = CloneFrame(master=self)
        right_frame.grid(row=0, column=1, sticky=N+E+S+W)
        left_frame.update_window_size()

        self.mainloop()


def display_clones(paths):
    """
    Display generated clone families in a CloneGUI.
    :param paths: a list of lists with paths to the .gif images
    """
    CloneGUI(paths).mainloop()


def build_clone_dict(families_dir):
    paths = [[] for family in listdir(families_dir) if 
                                     isdir(families_dir + '/' + family) and
                                     is_integer(family)]

    for current_family in range(len(paths)):
        clones = paths[current_family]
        clones.extend([(families_dir +
                        str(current_family) + 
                        '/' + clone + 
                        '/pattern.gif') for clone in listdir(families_dir +
                                                        str(current_family)) if
                                               isdir(families_dir +
                                                     str(current_family) +
                                                     '/' + clone) and is_integer(clone)])
    return paths


def is_integer(s):
    try:
        int(s)
        return True
    except ValueError:
        return False
 

if __name__ == "__main__":
    paths = build_clone_dict(sys.argv[1])
    display_clones(paths)
