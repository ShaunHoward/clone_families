import Tkinter as tk

from Tkinter import Frame, PhotoImage, Canvas, IntVar, OptionMenu, Button, Label
from math import ceil


class CloneFrame(Frame):

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.paths = self.master.paths
        self.current_family_index = 0
        self.current_clone_index = 0
        self.num_clones = len(self.paths[self.current_family_index])
        self.screen_width = master.winfo_screenwidth()
        self.screen_height = master.winfo_screenheight()

        self.current_image_file = PhotoImage(file=self.paths[self.current_family_index][self.current_clone_index])

        self.canvas = Canvas(self)
        self.canvas.pack(side='top', fill='both', expand='yes')

        self.current_image = self.canvas.create_image(
                                           0,
                                           0,
                                           image=self.current_image_file,
                                           anchor='nw')
        self.canvas.pack(side='top', fill='both', expand='yes')

        self.family_list = tuple(i for i in range(len(self.paths)))
        self.family_menu_variable = IntVar()
        self.family_menu_variable.set(self.family_list[self.current_family_index])
        
        family_menu_frame = Frame(self)
        family_menu_label = Label(family_menu_frame, text='Family: ')
        family_menu_label.pack(side=tk.LEFT)
        self.family_menu = OptionMenu(family_menu_frame,
                                      self.family_menu_variable,
                                      *self.family_list,
                                      command=self.set_current_family)
        self.family_menu.pack(side=tk.LEFT)
        family_menu_frame.pack()

        self.clone_list = tuple(i for i in range(self.num_clones))
        self.clone_menu_variable = IntVar()
        self.clone_menu_variable.set(self.clone_list[self.current_clone_index])
        
        clone_menu_frame = Frame(self)
        clone_menu_label = Label(clone_menu_frame, text='Clone: ')
        clone_menu_label.pack(side=tk.LEFT)
        self.clone_menu = OptionMenu(clone_menu_frame,
                                     self.clone_menu_variable,
                                     *self.clone_list,
                                     command=self.set_current_clone)
        self.clone_menu.pack(side=tk.LEFT)
        clone_menu_frame.pack()
        
        button_frame = Frame(self)
        self.prev_button = Button(button_frame,
                                  text='Previous',
                                  command=self.previous_image)
        self.prev_button.pack(side=tk.LEFT)
        self.next_button = Button(button_frame, text='Next', command=self.next_image)
        self.next_button.pack(side=tk.LEFT)
        button_frame.pack()

        self.update_image()

    def image_dimensions(self):
        """
        Returns the width and height of the current image in the frame.
        """
        return (self.current_image_file.width(),
                self.current_image_file.height())

    def set_current_family(self, value):
        """
        Callback for the family option menu. Sets the current family and resets
        the current clone to the first in that family.
        """
        self.current_family_index = value
        self.num_clones = len(self.paths[self.current_family_index])
        self.current_clone_index = 0
        self.clone_list = tuple(i for i in range(self.num_clones))
        menu = self.clone_menu['menu']
        menu.delete(0, 'end')

        for clone in self.clone_list:
            menu.add_command(label=clone,
                             command=lambda value=clone:
                             self.set_current_clone(value))

        self.update_image()

    def set_current_clone(self, value):
        """
        Callback for the clone option menu. Sets the current clone to the
        specified value.
        """
        self.current_clone_index = value
        self.update_image()

    def previous_image(self):
        """
        Callback for the 'Previous' button. Sets the current image to the
        previous one in that family, if possible.
        """
        if self.current_clone_index == 0:
            return
        else:
            self.current_clone_index -= 1
            self.update_image()

    def next_image(self):
        """
        Callback for the 'Next' button. Sets the current image to the next one,
        if possible.
        """
        if self.current_clone_index == self.num_clones - 1:
            return
        else:
            self.current_clone_index += 1
            self.update_image()

    def update_window_size(self):
        """
        Updates the window size to be big enough to show all clones in the
        window.
        """
        width, max_height = self.image_dimensions()

        for child in self.master.children.values():
            if not isinstance(child, CloneFrame) or child == self:
                continue
            child_width, child_height = child.image_dimensions()
            width += child_width
            if child_height > max_height:
                max_height = child_height

        self.master.geometry('{}x{}'.format(width, max_height + 250))

    def update_options_menus(self):
        """
        Updates the values in the clone and family options menus.
        """
        self.family_menu_variable.set(self.family_list[self.current_family_index])
        self.clone_menu_variable.set(self.clone_list[self.current_clone_index])

    def update_image(self):
        """
        Updates the image in this frame based on the values of various instance
        variables.
        """
        self.current_image_file = PhotoImage(
                                    file=self.paths[self.current_family_index]
                                                   [self.current_clone_index])
        if self.image_dimensions()[0] > self.screen_width / 2:
            resize_ratio = ceil(float(self.screen_width) /
                                self.image_dimensions()[0])
            self.current_image_file = self.current_image_file.subsample(int(resize_ratio))
        self.canvas.itemconfig(self.current_image,
                               image=self.current_image_file)
        self.update_options_menus()
        self.update_window_size()
