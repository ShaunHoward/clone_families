__author__ = 'Shaun Howard'

import os
from subprocess import check_call, CalledProcessError
from utilities import graph_utils, io_utils


def render_dot_as_png(input_file_path, output_file_path):
    try:
        check_call(['dot', '-Tpng', input_file_path, '-o', output_file_path + '.png'])
    except CalledProcessError:
        print "Could not render sub-graph as png: " + input_file_path


def render_dot_as_gif(input_file_path, output_dir, file_path):
    rendered_file_path = '/'.join([output_dir, file_path + '.gif'])
    try:
        if not os.path.exists(output_dir):
            os.mkdir(output_dir)
        check_call(['dot', '-Tgif', input_file_path, '-o', rendered_file_path])
    except CalledProcessError:
        print "Could not render sub-graph as gif: " + input_file_path
    return rendered_file_path


def render_clone_families(clone_families, graph_dir_root, family_dir):
    clone_family_file_paths = []

    local_family_dir = family_dir

    # delete the rendering directory if it exists
    io_utils.delete_if_exists(local_family_dir)
    # create the rendering directory
    io_utils.create(local_family_dir)

    family_num = 0
    for clone_family in clone_families:
        clone_family_file_paths.append([])
        for clone in clone_family:
            # create the base output directory from various family attributes
            base_output_dir = '/'.join([local_family_dir, str(family_num), str(clone)])

            # make the base output directory if it does not exist
            if not os.path.exists(base_output_dir):
                os.makedirs(base_output_dir)

            # write pattern graph image
            pattern_graph_path = graph_utils.get_pattern_graph_path(clone, graph_dir_root)
            rendered_pattern_path = render_dot_as_gif(pattern_graph_path, base_output_dir, 'pattern')

            # add the rendered image path to the list for this clone family
            clone_family_file_paths[family_num].append(rendered_pattern_path)

            # write embedded graph images
            embedded_paths = graph_utils.get_embedded_graph_paths(clone, graph_dir_root)
            embedded_graph_num = 0
            for path in embedded_paths:
                # read in dot graph for editing
                embedded_graph_string = io_utils.read_file_contents(path)

                # add title with package and class name to graph
                embedded_graph_string = add_title_to_embedded_graph(embedded_graph_string)

                # write new embedded graph to file with title
                temp_path = path + ".tmp"
                temp_graph = open(temp_path, 'w')
                temp_graph.write(embedded_graph_string)
                temp_graph.close()

                # render new graph with title as gif (or png)
                render_dot_as_gif(temp_path, base_output_dir, 'embedding_' + str(embedded_graph_num))
                # render_dot_as_png(temp_path, embedded_output_file)

                # delete temporary graph file
                os.remove(temp_path)
                embedded_graph_num += 1
        family_num += 1

    return clone_family_file_paths, local_family_dir


def add_title_to_embedded_graph(graph_string):
    graph_title = create_embedded_graph_title(graph_string)
    title_loc = '    labelloc="t";'
    # find end of dot graph string
    end_of_graph = graph_string.rfind('}')
    title_ins_point = end_of_graph - 1
    graph_string = '\n'.join([graph_string[:title_ins_point], title_loc, graph_title, graph_string[end_of_graph:]])
    return graph_string


def create_embedded_graph_title(graph_string):
    # package_name = find_value_with_name('package_name', graph_string)
    class_name = find_value_with_name('class_name', graph_string)
    # graph_title = '.'.join([package_name, class_name])
    graph_title = ''.join(['    label=\"', class_name, '\";'])
    return graph_title


def find_value_with_name(field_name, string):
    """
    Finds the specified field value in the provided string.
    :param field_name: the name of the field to look for in the provided string
    :param string: the string to search for the provided field and its value in
    :return: the value of the string
    """
    field = ''.join([field_name, '=\"'])
    field_start = string.index(field)
    field_end = field_start + len(field)
    val_start = field_end
    modified_str = string[field_end:]
    val_end = val_start + modified_str.index('\"')
    val = string[val_start:val_end]
    return val
