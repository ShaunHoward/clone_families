# README #

This README documents how to setup our framework which clusters similar frequent subgraphs of Java program dependence graphs.

Use a virtual environment and install requirements.txt using pip.

Please refer to installation notes below to get everything set up.

Make sure all requirements are installed.

Source your virtual environment and run our tool with the following arguments:

    -a or --archive : the tar.gz archive to read graphs from (if the graph destination given does not exist)
    -gd or --graph_directory: the graph directory to un-tar the given archive to or read from (if exists)
    -od or --output_directory: the directory to render code clone families to in an indexed order
    -ss or --similarity_strategy: the similarity strategy to use during code clone detection
    (ss may be "Levenshtein", "Kmeans", "Kmedoids", "Isomorphic Kernel", or "Random Walk Kernel")
    -v or --visual: whether to render and display code clones after processing
    -t or --time: whether to time the code clone detection and clustering algorithm

Only a certain number of parameters must be given based on what seems logical,
i.e. give an archive and graph directory if you only have an archive of graphs that need to be extracted.

The default similarity strategy used is the graph Levenshtein edit distance.
Timing and visualization are offered by default (specify false like -v=False or -t=False otherwise).

Example usage (from command-line):
-Navigate to "clone_families" directory
-Run a command like the following:
python clone_commander.py -a=/home/user/projects/eecs493/clone_families/training_data/jgit.tar.gz
 -gd=/home/user/projects/eecs493/clone_families/training_data/jgit_subset
 -od=/home/user/projects/eecs493/clone_families/output_families/iso_families_test
 -ss="isomorphic kernel"

Be patient, code clone detection and clustering may take hours depending on data set size and graph size.

Print-outs will be published to the console as progress is made by the tool. Following the print-outs to
determine if the program is working correctly or if something has messed up.

DISCLAIMER: This tool may use up a lot of memory on your hard disk. Please make sure you have sufficient disk
space before performing code clone rendering (-v) or archive extraction (-a). Also, make sure your computer
has sufficient hardware meeting the following criteria:

-Quad-core processor (at least 2.0 ghz)
-8 GB of RAM (less may cause crashes or freeze ups)
-128 GB hard disk (preferably solid-state) with at least 2 GB free (depending on usage and size of data set)

Feel free to contact us if you need any help:

* Shaun Howard, smh150@case.edu
* Ben Marks, bjm114@case.edu
* Rebecca Frederick, rmf61@case.edu
* Emilio Colindres, exc231@case.edu

INSTALLATION NOTES using PyCharm (may need updating):

First thing’s first. In order to get started on this project, you will need to do a couple of things. The following are useful suggestions in order to get into a working development environment:

1. Install Ubuntu Linux 14.04 LTS or use a VM with this distro installed

2. Set up Java: https://www.digitalocean.com/community/tutorials/how-to-install-java-on-ubuntu-with-apt-get

3. Set up PyCharm: http://exponential.io/blog/2015/02/10/install-pycharm-on-ubuntu-linux/

Note that the version in the link is out-dated but the same directions apply, just change the version number in all steps to the latest version that you have downloaded.

4. Make a symlink to PyCharm:
This is easily done by just running PyCharm and then clipping it to your task bar.

5. Make sure python is installed (2.7) and then run the following:

    sudo apt-get install gcc libatlas-base-dev gfortran python-pip python-dev build-essential setuptools \ python-numpy python-scipy libatlas-dev libatlas3gf-base
    sudo apt-get install
    Run the following commands:
    which gcc; which g++; Make sure the output makes sense.
    export CC=$(which gcc)
    export CXX=$(which g++)
    echo $CC $CXX (smoke test)
    pip install setuptools
    pip install -U pip setuptools
    pip install virtualenv

6. Make project directories:

    cd ~
    mkdir projects
    cd projects
    mkdir eecs493
    cd eecs493
    virtualenv venv
    source venv/bin/activate

7. Clone repos used in project:

    cd ~/projects/eecs493
    git clone https://ShaunHoward@bitbucket.org/<username>/clone_families.git
    git clone https://github.com/timtadh/dot_tools.git

7. Install dot_tools to venv

    cd ~/projects/eecs493
    source venv/bin/activate
    cd dot_tools
    python setup.py install

8. Set up project in PyCharm

    Boot up PyCharm
    Choose to File -> Open and navigate to ~/projects/eecs493/clone_families
    PyCharm will figure out the structure for you
    Go to File -> Settings -> Project: clone_families -> Project Interpreter
    Click on the cog near the current interpreter path and select “Add Local”
    Navigate to ~/projects/eecs493/venv/bin/python2.7
    Click “Apply” and “Okay”

8. Set up venv with packages

    set -o vi
    source venv/bin/activate
    Note that some of these installations may take a while:
    pip install -r requirements.txt (scipy, matplotlib, scikit-learn)

9. Add graph viz for graph visualization

    sudo apt-get install graphviz

