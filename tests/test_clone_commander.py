__author__ = 'shaun'

import unittest

import os

from dot_tools.dot_graph import SimpleGraph

import clone_commander as cc
from similarity_strategy.similarity_strategies import ISO_KERNEL, KMEANS, LED, RW_KERNEL, KMEDOIDS

DIR = os.path.dirname(__file__)
TRAINING = os.path.abspath(os.path.join(DIR, '..', 'training_data'))
GRAPH_DIR = os.path.join(TRAINING, 'jgit_subset')
GRAPH_ARCHIVE = os.path.join(TRAINING, 'jgit_test.tar.gz')


class TestCloneCommander(unittest.TestCase):

    def assert_is_triple_graph(self, graph):
        self.assertEqual(type(graph), list)
        self.assertGreater(len(graph), 0)
        self.assertEqual(type(graph[0]), tuple)

    def assert_is_simple_graph(self, graph):
        self.assertEqual(type(graph), SimpleGraph)
        self.assertGreater(len(graph.nodes), 0)
        self.assertGreater(len(graph.edges), 0)

    def assert_is_graph_db(self, graph_db, check_graph_method):
        self.assertEqual(type(graph_db), type(()))  # tuple
        self.assertEqual(len(graph_db), 2)
        self.assertEqual(type(graph_db[0]), type([]))  # list
        self.assertGreater(len(graph_db[0]), 0)
        for graph in graph_db[0]:
            check_graph_method(graph)
        self.assertEqual(type(graph_db[1]), type(" "))

    def test_build_graph_db_triples(self):
        graph_db = cc.build_graph_db(GRAPH_ARCHIVE, GRAPH_DIR, LED)
        self.assert_is_graph_db(graph_db, self.assert_is_triple_graph)

    def test_build_graph_db_simple(self):
        graph_db = cc.build_graph_db(GRAPH_ARCHIVE, GRAPH_DIR, ISO_KERNEL)
        self.assert_is_graph_db(graph_db, self.assert_is_simple_graph)

    def test_determine_similarity_metric_levenshstein(self):
        # test the four possible cases for sim metric
        sim_metric = cc.determine_similarity_strategy("levenshstein")
        self.assertEqual(sim_metric, LED)

    def test_determine_similarity_metric_default(self):
        # test no or extraneous input (should be LED)
        sim_metric = cc.determine_similarity_strategy("   ")
        self.assertEqual(sim_metric, LED)

    def test_determine_similarity_metric_iso_kernel(self):
        sim_metric = cc.determine_similarity_strategy("iso kernel")
        self.assertEqual(sim_metric, ISO_KERNEL)

    def test_determine_similarity_metric_rw_kernel(self):
        sim_metric = cc.determine_similarity_strategy("random kernel")
        self.assertEqual(sim_metric, RW_KERNEL)

    def test_determine_similarity_metric_kmeans(self):
        sim_metric = cc.determine_similarity_strategy("kmeans")
        self.assertEqual(sim_metric, KMEANS)

    def test_determine_similarity_metric_kmedoids(self):
        sim_metric = cc.determine_similarity_strategy("kmedoids")
        self.assertEqual(sim_metric, KMEDOIDS)


if __name__ == '__main__':
    unittest.main()
