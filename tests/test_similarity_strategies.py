import unittest

import os

import numpy as np

import clone_commander as cc
from similarity_strategy import similarity_strategies as sim
from similarity_strategy.similarity_strategies import KMEANS, LED, RW_KERNEL, ISO_KERNEL
import similarity_strategy.levenshstein_strategy as ls
import similarity_strategy.kernel_strategy as ks
from similarity_strategy.kernel_strategy import RandomWalkKernelStrategy as rwks

DIR = os.path.dirname(__file__)
TRAINING = os.path.abspath(os.path.join(DIR, '..', 'training_data'))
GRAPH_DIR = os.path.join(TRAINING, 'jgit_subset')
GRAPH_ARCHIVE = os.path.join(TRAINING, 'jgit.tar.gz')

graph_db_led = cc.build_graph_db(GRAPH_ARCHIVE, GRAPH_DIR, LED)[0]
graph_db_iso = cc.build_graph_db(GRAPH_ARCHIVE, GRAPH_DIR, ISO_KERNEL)[0]
graph_db_rw = graph_db_iso
graph_db_kmeans = graph_db_iso

len_graph_db = len(graph_db_led)

rwk = rwks(graph_db_rw)

led_context = {"similarity_method": ls.graph_comparison,
               "complement_scores": True,
               "epsilon": 0.01,
               "graph_style": "triples",
               "kernel": 0,
               "normalize": 1}

iso_kernel_context = {"similarity_method": ks.isomorphic_kernel_graph_comparison,
                      "complement_scores": False,
                      "epsilon": 0.00005,
                      "graph_style": "simple_graph",
                      "kernel": 1,
                      "normalize": 0}

rw_kernel_context = {"similarity_method": rwk.graph_comparison,
                     "complement_scores": True,
                     "epsilon": 0.1,
                     "graph_style": "simple_graph",
                     "kernel": 1,
                     "normalize": 1}

kmeans_context = {"complement_scores": True,
                  "epsilon": 0.05,
                  "graph_style": "simple_graph",
                  "num_clusters": 20,
                  "init_type": "k-means++",
                  "kernel": 0,
                  "normalize": 1}

sims = [LED, ISO_KERNEL, RW_KERNEL, KMEANS]
graph_dbs_by_sim = {LED: graph_db_led, RW_KERNEL: graph_db_rw, ISO_KERNEL: graph_db_iso, KMEANS: graph_db_kmeans}
contexts_by_sim = {LED: led_context, ISO_KERNEL: iso_kernel_context, RW_KERNEL: rw_kernel_context, KMEANS: kmeans_context}

# PROBLEM: bound vs unbound method for rwks -> needs fixed


class TestSimilarityStrategies(unittest.TestCase):

    def test_jaccard_similarity_matrix(self):
        # triple graph 'a', triple graph 'b' -> float [0, 1]
        # test case 1 - known that a is more similar to b than to c (a and b are in same clone family as per LED)
        a = graph_db_led[32]
        b = graph_db_led[52]
        c = graph_db_led[48]
        result_ab = sim.jaccard_similarity_matrix(a, b)
        result_ac = sim.jaccard_similarity_matrix(a, c)
        self.assertLess(result_ac, result_ab)
        # test case 2 - known that a == b
        result_aa = sim.jaccard_similarity_matrix(a, a)
        self.assertEqual(result_aa, 1.0)

    # NOTE: multiply_scores(matrix, amount) is not used anywhere

    def test_generate_strategy_context(self):
        self.maxDiff = None
        for similarity_strategy in sims:
            context = sim.generate_strategy_context(similarity_strategy, graph_dbs_by_sim[similarity_strategy])
            if similarity_strategy == RW_KERNEL:
                # need to do this because 'similarity_method' is a class instance
                self.assertItemsEqual(context.keys(), contexts_by_sim[similarity_strategy].keys())
                self.assertEqual(type(context['similarity_method']), type(rwks(graph_db_rw).graph_comparison))
                self.assertEqual(context['complement_scores'], contexts_by_sim[similarity_strategy]['complement_scores'])
                self.assertEqual(context['epsilon'], contexts_by_sim[similarity_strategy]['epsilon'])
                self.assertEqual(context['graph_style'], contexts_by_sim[similarity_strategy]['graph_style'])
                self.assertEqual(context['kernel'], contexts_by_sim[similarity_strategy]['kernel'])
                self.assertEqual(context['normalize'], contexts_by_sim[similarity_strategy]['normalize'])
            else:
                self.assertDictEqual(context, contexts_by_sim[similarity_strategy])

    def test_generate_similarity_scores(self):
        for similarity_strategy in sims:
            if similarity_strategy == KMEANS: continue
            # if similarity_strategy == RW_KERNEL: continue
            scores = sim.generate_similarity_scores(graph_dbs_by_sim[similarity_strategy], contexts_by_sim[similarity_strategy])
            self.assertEqual(len(scores), len_graph_db)
            for i in range(len_graph_db):
                for j in range(len_graph_db):
                    self.assertGreaterEqual(scores[i][j], 0.0)
                    self.assertLessEqual(scores[i][j], 1.0)

    # commentary: if min==max, shouldn't the normal value be 1.0?
    def test_normalize(self):
        scores = np.matrix([[float(x) for x in range(10)] for y in range(10)])
        # test normalize data by column
        norm_scores = sim.normalize(scores, column_wise=True)
        self.assertTupleEqual(norm_scores.shape, (10, 10))
        for i in range(norm_scores.shape[0]):
            for j in range(norm_scores.shape[1]):
                self.assertEqual(norm_scores[i, j], float(j))
        # test normalize data by entire patterns
        scores = np.matrix([[float(x) for x in range(10)] for y in range(10)])
        norm_scores = sim.normalize(scores, column_wise=False)
        self.assertTupleEqual(norm_scores.shape, (10, 10))
        for i in range(norm_scores.shape[0]):
            for j in range(norm_scores.shape[1]):
                self.assertEqual(norm_scores[i, j], float(j)/9.0)

        scores = np.matrix([[float(y) for x in range(10)] for y in range(10)])
        # test normalize data by column
        norm_scores = sim.normalize(scores, column_wise=False)
        self.assertTupleEqual(norm_scores.shape, (10, 10))
        for i in range(norm_scores.shape[0]):  # all rows
            for j in range(norm_scores.shape[1]):  # all columns
                self.assertEqual(norm_scores[i, j], float(i)/9.0)
        # test normalize data by entire patterns
        scores = np.matrix([[float(y) for x in range(10)] for y in range(10)])
        norm_scores = sim.normalize(scores, column_wise=True)
        self.assertTupleEqual(norm_scores.shape, (10, 10))
        for i in range(norm_scores.shape[0]):
            for j in range(norm_scores.shape[1]):
                self.assertEqual(norm_scores[i, j], float(i)/9.0)

    def test_graph_similarity(self):
        for similarity_strategy in sims:
            if similarity_strategy == KMEANS: continue
            # if similarity_strategy == RW_KERNEL: continue
            similar_graphs = sim.graph_similarity(graph_dbs_by_sim[similarity_strategy], contexts_by_sim[similarity_strategy])
            self.assertEqual(type(similar_graphs), type([]))
            self.assertEqual(type(similar_graphs[0]), type(()))
            for pair in similar_graphs:
                self.assertNotIn((pair[1], pair[0]), similar_graphs)


if __name__ == '__main__':
    unittest.main()
