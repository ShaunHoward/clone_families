__author__ = 'shaunhoward'

import unittest

import os

from numpy import ndarray, float64, int64
from sklearn.cluster import KMeans
from sklearn.preprocessing import scale

import clone_commander as cc
from utilities import graph_utils as g_util
from utilities import io_utils
from similarity_strategy.similarity_strategies import EPSILON, INIT_TYPE, NUM_CLUSTERS
from similarity_strategy import kmeans_strategy as kmeans
from similarity_strategy.similarity_strategies import KMEANS, generate_strategy_context

DIR = os.path.dirname(__file__)
TRAINING = os.path.abspath(os.path.join(DIR, '..', 'training_data'))
GRAPH_DIR = os.path.join(TRAINING, 'jgit_subset')
GRAPH_ARCHIVE = os.path.join(TRAINING, 'jgit_test.tar.gz')


class TestKmeansStrategy(unittest.TestCase):

    def test_find_kmeans_clusters(self):
        graph_db = self.build_graph_db()
        clusters = kmeans.compute_kmeans_clusters(graph_db, generate_strategy_context(KMEANS, graph_db))
        self.assertIsNotNone(clusters)
        self.assertTrue(type(clusters), list)
        self.assertGreater(len(clusters), 0)
        for cluster in clusters:
            self.assertIsNotNone(cluster)
            self.assertTrue(type(cluster), list)
            self.assertGreater(len(cluster), 0)

    def test_get_clusters(self):
        patterns = kmeans.create_patterns_from_graphs(self.build_graph_db())
        self.assertIsNotNone(patterns)
        patterns = scale(patterns)
        strategy_context = generate_strategy_context(KMEANS, graph_db=None)
        k_means = KMeans(n_clusters=strategy_context[NUM_CLUSTERS],
                         init=strategy_context[INIT_TYPE],
                         tol=strategy_context[EPSILON])
        k_means.fit_predict(patterns)
        clusters = kmeans.get_clusters(k_means)
        self.assertIsNotNone(clusters)
        self.assertEqual(type(clusters), list)
        self.assertGreater(len(clusters), 0)

    def test_create_patterns_from_graphs(self):
        graph_db = self.build_graph_db()
        patterns = kmeans.create_patterns_from_graphs(graph_db)
        self.assertEqual(type(patterns), ndarray)
        self.assertEqual(patterns.shape[0], 100)
        self.assertEqual(patterns.shape[1], 11)
        self.assertEqual(type(patterns[0][0]), float64)

    def test_get_graph_attributes(self):
        graph_db = self.get_first_graph()
        graph = graph_db[0]
        attribs = kmeans.get_graph_attributes(graph)
        self.assertIsNotNone(attribs)
        self.assertEqual(type(attribs), dict)
        self.assertGreater(len(attribs), 0)
        for attrib in attribs.keys():
            self.assertIsNotNone(attrib)
            attrib_val = attribs[attrib]
            self.assertIsNotNone(attrib_val)
            # check if the attribs are either float or float64 depending on precision
            self.assertTrue(type(attrib_val) == float or
                            type(attrib_val) == float64)
            # values must be greater than or equal to a float value of 0
            self.assertGreaterEqual(attrib_val, 0.0)

    def test_avg_in_degree_rate(self):
        graph_db = self.get_first_graph()
        adj_mat = g_util.get_adjacency_matrix(graph_db[0].nodes, graph_db[0].edges)
        avg_in_degree_rate = kmeans.avg_in_degree_rate(adj_mat)
        self.assertEqual(type(avg_in_degree_rate), float64)
        self.assertGreaterEqual(avg_in_degree_rate, 0)
        self.assertAlmostEqual(avg_in_degree_rate, 0.098765, 5)

    def test_min_in_degree(self):
        graph_db = self.get_first_graph()
        adj_mat = g_util.get_adjacency_matrix(graph_db[0].nodes, graph_db[0].edges)
        min_in_degree = kmeans.min_in_degree(adj_mat)
        self.assertEqual(type(min_in_degree), int64)
        self.assertGreaterEqual(min_in_degree, 0)

    def test_max_in_degree(self):
        graph_db = self.get_first_graph()
        adj_mat = g_util.get_adjacency_matrix(graph_db[0].nodes, graph_db[0].edges)
        max_in_degree = kmeans.max_in_degree(adj_mat)
        self.assertEqual(type(max_in_degree), int64)
        self.assertGreaterEqual(max_in_degree, 0)
        self.assertEqual(max_in_degree, 2)

    def test_min_out_degree(self):
        graph_db = self.get_first_graph()
        adj_mat = g_util.get_adjacency_matrix(graph_db[0].nodes, graph_db[0].edges)
        min_out_degree = kmeans.min_out_degree(adj_mat)
        self.assertEqual(type(min_out_degree), int64)
        self.assertGreaterEqual(min_out_degree, 0)

    def test_max_out_degree(self):
        graph_db = self.get_first_graph()
        adj_mat = g_util.get_adjacency_matrix(graph_db[0].nodes, graph_db[0].edges)
        max_out_degree = kmeans.max_out_degree(adj_mat)
        self.assertEqual(type(max_out_degree), int64)
        self.assertGreaterEqual(max_out_degree, 2)

    def test_get_shortest_path_attributes(self):
        graph_db = self.get_first_graph()
        adj_mat = g_util.get_adjacency_matrix(graph_db[0].nodes, graph_db[0].edges)
        max_in_degree = kmeans.max_in_degree(adj_mat)
        self.assertEqual(type(max_in_degree), int64)
        self.assertGreaterEqual(max_in_degree, 2)

    def test_get_density(self):
        graph_db = self.get_first_graph()
        density = kmeans.get_density(graph_db[0])
        self.assertEqual(type(density), float)
        self.assertGreaterEqual(density, 0)
        self.assertAlmostEqual(density, 0.222, 3)

    def get_first_graph(self, make_triples=False):
        graph_file_paths = io_utils.get_dot_file_paths(GRAPH_DIR, 'pattern.dot')[0]

        # first, read the string of the graph
        graph_string = io_utils.read_file_contents(graph_file_paths[0])

        # second, parse the string into a simple graph form
        simple_graph = g_util.parse_simple_graph(graph_string)

        # either make the graph into triples or an adjacency list
        # ordering of triples is produced by the Bliss algorithm
        # triples represent canonical labeling of graphs
        if make_triples:
            graph = g_util.triples(simple_graph)
        else:
            # keep the simple graph representation and use the edges and nodes lists
            graph = simple_graph

        # facade for a graph db
        return [graph]

    def build_graph_db(self):
        return cc.build_graph_db(GRAPH_ARCHIVE, GRAPH_DIR, KMEANS)[0]
