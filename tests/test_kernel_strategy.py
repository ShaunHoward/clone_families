import unittest

import os

from dot_tools.dot_graph import SimpleGraph
import numpy

import clone_commander as cc
from similarity_strategy import kernel_strategy as k_s
from similarity_strategy.similarity_strategies import RW_KERNEL, ISO_KERNEL

DIR = os.path.dirname(__file__)
TRAINING = os.path.abspath(os.path.join(DIR, '..', 'training_data'))
GRAPH_DIR = os.path.join(TRAINING, 'jgit_subset')
GRAPH_ARCHIVE = os.path.join(TRAINING, 'jgit_test.tar.gz')


class TestKernelStrategy(unittest.TestCase):

    def test_graph_comparison(self):
        graph_db = self.build_graph_db(RW_KERNEL)[0]
        rwks = k_s.RandomWalkKernelStrategy(graph_db)
        g1 = SimpleGraph()
        g2 = SimpleGraph()
        scores = rwks.graph_comparison(graph_db)
        self.assertTupleEqual(scores.shape, (len(graph_db), len(graph_db)))
        self.assertEqual(type(scores), numpy.ndarray)
        for score_list in scores:
            for score in score_list:
                self.assertEqual(type(score), numpy.float64)
                self.assertGreaterEqual(score, 0.0)

    def test_isomorphic_kernel_graph_comparison(self):
        g1 = SimpleGraph()
        g2 = SimpleGraph()
        num_sub_graph_vertices = 4
        graph_db = self.build_graph_db(ISO_KERNEL)[0]
        scores = k_s.isomorphic_kernel_graph_comparison(graph_db, num_sub_graph_vertices)
        self.assertEqual(type(scores), numpy.ndarray)
        self.assertTupleEqual(scores.shape, (len(graph_db), len(graph_db)))
        for score_list in scores:
            for score in score_list:
                self.assertEqual(type(score), numpy.float64)
                self.assertGreater(score, 0)
                self.assertLessEqual(score, 1.0)

    def test_get_labels(self):
        graph_db = self.build_graph_db(RW_KERNEL)
        labels = k_s.get_labels(graph_db[0])
        self.assertEqual(type(labels), list)
        for label in labels:
            self.assertEqual(type(label), str)

    def test_get_label_adjacency_label(self):
        graph = SimpleGraph()
        labels = self.get_graph_labels()
        result = k_s.get_label_adjacency_label(graph, labels)
        self.assertEqual(type(result), numpy.ndarray)
        self.assertEqual(len(result), len(labels))
        for row in result:
            self.assertEqual(type(row), numpy.ndarray)
            self.assertEqual(len(row), len(labels))
            for num in row:
                self.assertEqual(type(num), numpy.int64)
                self.assertGreaterEqual(num, 0)

    def test_get_label_matrix(self):
        labels = self.get_graph_labels()
        nodes = {71489.0: {'idx': '66571', 'label': '+'}, 71490.0: {'idx': '66568', 'label': "const '1'"},
                 71491.0: {'idx': '66569', 'label': 'call org.eclipse.jgit.util.IntList.get'},
                 71474.0: {'idx': '66687', 'label': 'param 2'}, 71475.0: {'idx': '66686', 'label': 'param 3'},
                 71478.0: {'idx': '66683', 'label': '+'}, 71479.0: {'idx': '66682', 'label': "const '1'"},
                 71482.0: {'idx': '66679', 'label': 'org.eclipse.jgit.diff.RawText.lines'},
                 71483.0: {'idx': '66678', 'label': 'call org.eclipse.jgit.util.IntList.get'}}
        result = k_s.get_label_matrix(nodes, labels)
        self.assertEqual(type(result), numpy.ndarray)
        self.assertEqual(len(result), len(labels))
        for row in result:
            self.assertEqual(type(row), numpy.ndarray)
            self.assertEqual(len(row), len(nodes))
            for num in row:
                self.assertEqual(type(num), numpy.int64)
                self.assertEqual(num == 0 or num == 1, True)

    def test_get_isomorphic_kernel_scores(self):
        sub_graphs_1 = [{'count': 0.1746031746031746, 'subgraph': {'edges': [(71490.0, 71489.0, 'int:2'), (71489.0, 71491.0, 'int:1')], 'vertices': (71489.0, 71490.0, 71491.0, 71474.0), 'node_supplies': [-1, 0, 0, 1]}}, {'count': 0.03968253968253968, 'subgraph': {'edges': [(71490.0, 71489.0, 'int:2'), (71478.0, 71489.0, 'int:1'), (71489.0, 71491.0, 'int:1')], 'vertices': (71489.0, 71490.0, 71491.0, 71478.0), 'node_supplies': [-1, -1, 1, 1]}}, {'count': 0.373015873015873, 'subgraph': {'edges': [(71490.0, 71489.0, 'int:2')], 'vertices': (71489.0, 71490.0, 71474.0, 71475.0), 'node_supplies': [-1, 0, 0, 1]}}, {'count': 0.06349206349206349, 'subgraph': {'edges': [(71490.0, 71489.0, 'int:2'), (71478.0, 71489.0, 'int:1')], 'vertices': (71489.0, 71490.0, 71474.0, 71478.0), 'node_supplies': [-2, 0, 1, 1]}}, {'count': 0.05555555555555555, 'subgraph': {'edges': [(71490.0, 71489.0, 'int:2'), (71474.0, 71482.0, 'org.eclipse.jgit.diff.RawText:1')], 'vertices': (71489.0, 71490.0, 71474.0, 71482.0), 'node_supplies': [-1, -1, 1, 1]}}, {'count': 0.03968253968253968, 'subgraph': {'edges': [(71490.0, 71489.0, 'int:2'), (71475.0, 71478.0, 'int:1'), (71478.0, 71489.0, 'int:1')], 'vertices': (71489.0, 71490.0, 71475.0, 71478.0), 'node_supplies': [-2, 0, 1, 1]}}, {'count': 0.015873015873015872, 'subgraph': {'edges': [(71490.0, 71489.0, 'int:2'), (71478.0, 71489.0, 'int:1'), (71478.0, 71483.0, 'int:1')], 'vertices': (71489.0, 71490.0, 71478.0, 71483.0), 'node_supplies': [-2, -1, 1, 2]}}, {'count': 0.015873015873015872, 'subgraph': {'edges': [(71475.0, 71478.0, 'int:1'), (71478.0, 71489.0, 'int:1'), (71489.0, 71491.0, 'int:1')], 'vertices': (71489.0, 71491.0, 71475.0, 71478.0), 'node_supplies': [-1, 0, 0, 1]}}, {'count': 0.007936507936507936, 'subgraph': {'edges': [(71478.0, 71489.0, 'int:1'), (71489.0, 71491.0, 'int:1'), (71478.0, 71483.0, 'int:1')], 'vertices': (71489.0, 71491.0, 71478.0, 71483.0), 'node_supplies': [-1, -1, 0, 2]}}, {'count': 0.20634920634920634, 'subgraph': {'edges': [], 'vertices': (71489.0, 71474.0, 71475.0, 71479.0), 'node_supplies': [0, 0, 0, 0]}}, {'count': 0.007936507936507936, 'subgraph': {'edges': [(71478.0, 71489.0, 'int:1'), (71478.0, 71483.0, 'int:1')], 'vertices': (71489.0, 71474.0, 71478.0, 71483.0), 'node_supplies': [-1, -1, 0, 2]}}]
        sub_graphs_2 = [{'count': 0.1746031746031746, 'subgraph': {'edges': [(71490.0, 71489.0, 'int:2'), (71489.0, 71491.0, 'int:1')], 'vertices': (71489.0, 71490.0, 71491.0, 71474.0), 'node_supplies': [-1, 0, 0, 1]}}, {'count': 0.03968253968253968, 'subgraph': {'edges': [(71490.0, 71489.0, 'int:2'), (71478.0, 71489.0, 'int:1'), (71489.0, 71491.0, 'int:1')], 'vertices': (71489.0, 71490.0, 71491.0, 71478.0), 'node_supplies': [-1, -1, 1, 1]}}, {'count': 0.373015873015873, 'subgraph': {'edges': [(71490.0, 71489.0, 'int:2')], 'vertices': (71489.0, 71490.0, 71474.0, 71475.0), 'node_supplies': [-1, 0, 0, 1]}}, {'count': 0.06349206349206349, 'subgraph': {'edges': [(71490.0, 71489.0, 'int:2'), (71478.0, 71489.0, 'int:1')], 'vertices': (71489.0, 71490.0, 71474.0, 71478.0), 'node_supplies': [-2, 0, 1, 1]}}, {'count': 0.05555555555555555, 'subgraph': {'edges': [(71490.0, 71489.0, 'int:2'), (71474.0, 71482.0, 'org.eclipse.jgit.diff.RawText:1')], 'vertices': (71489.0, 71490.0, 71474.0, 71482.0), 'node_supplies': [-1, -1, 1, 1]}}, {'count': 0.03968253968253968, 'subgraph': {'edges': [(71490.0, 71489.0, 'int:2'), (71475.0, 71478.0, 'int:1'), (71478.0, 71489.0, 'int:1')], 'vertices': (71489.0, 71490.0, 71475.0, 71478.0), 'node_supplies': [-2, 0, 1, 1]}}, {'count': 0.015873015873015872, 'subgraph': {'edges': [(71490.0, 71489.0, 'int:2'), (71478.0, 71489.0, 'int:1'), (71478.0, 71483.0, 'int:1')], 'vertices': (71489.0, 71490.0, 71478.0, 71483.0), 'node_supplies': [-2, -1, 1, 2]}}, {'count': 0.015873015873015872, 'subgraph': {'edges': [(71475.0, 71478.0, 'int:1'), (71478.0, 71489.0, 'int:1'), (71489.0, 71491.0, 'int:1')], 'vertices': (71489.0, 71491.0, 71475.0, 71478.0), 'node_supplies': [-1, 0, 0, 1]}}, {'count': 0.007936507936507936, 'subgraph': {'edges': [(71478.0, 71489.0, 'int:1'), (71489.0, 71491.0, 'int:1'), (71478.0, 71483.0, 'int:1')], 'vertices': (71489.0, 71491.0, 71478.0, 71483.0), 'node_supplies': [-1, -1, 0, 2]}}, {'count': 0.20634920634920634, 'subgraph': {'edges': [], 'vertices': (71489.0, 71474.0, 71475.0, 71479.0), 'node_supplies': [0, 0, 0, 0]}}, {'count': 0.007936507936507936, 'subgraph': {'edges': [(71478.0, 71489.0, 'int:1'), (71478.0, 71483.0, 'int:1')], 'vertices': (71489.0, 71474.0, 71478.0, 71483.0), 'node_supplies': [-1, -1, 0, 2]}}]
        score = k_s.get_isomorphic_kernel_scores(sub_graphs_1, sub_graphs_2)
        self.assertEqual(type(score), float)
        self.assertGreater(score, 0)
        self.assertEqual(score, 1.0)#1.0/math.sqrt(math.pi)**11)

    def test_get_random_walk_kernel_scores(self):
        pass

    def test_get_sub_graphs(self):
        graph = SimpleGraph()
        num_vertices_in_sub_graphs = 4
        sub_graphs = k_s.get_sub_graphs(graph, num_vertices_in_sub_graphs)
        self.assertEqual(type(sub_graphs), list)
        for sub_graph in sub_graphs:
            self.assertEqual(type(sub_graph), dict)
            self.assertEqual(type(sub_graph["count"]), float)
            self.assertEqual(type(sub_graph["subgraph"]), dict)
            self.assertEqual(type(sub_graph["subgraph"]["edges"]), list)
            for edge in sub_graph["subgraph"]["edges"]:
                self.assertEqual(type(edge), tuple)
                self.assertEqual(len(edge), 3)
            self.assertEqual(type(sub_graph["subgraph"]["node_supplies"]), list)
            for supply in sub_graph["subgraph"]["node_supplies"]:
                self.assertEqual(type(supply), int)
            self.assertEqual(type(sub_graph["subgraph"]["vertices"]), tuple)
            for vertex in sub_graph["subgraph"]["vertices"]:
                self.assertEqual(type(vertex), float)

    def test_is_isomorphic(self):
        graph1 = {'edges': [(71490.0, 71489.0, 'int:2'), (71489.0, 71491.0, 'int:1')],
                  'vertices': (71489.0, 71490.0, 71491.0, 71475.0), 'node_supplies': [-1, 0, 0, 1]}
        graph2 = {'edges': [(71490.0, 71489.0, 'int:2'), (71489.0, 71491.0, 'int:1')],
                  'vertices': (71489.0, 71490.0, 71491.0, 71474.0), 'node_supplies': [-1, 0, 0, 1]}
        result = k_s.is_isomorphic(graph1,graph2)
        self.assertEqual(type(result),bool)
        self.assertEqual(result,True)
        # test unequal number of edges

        # test different "node supplies"

        # test known isomorphic pair

    def build_graph_db(self, sim_metric):
        return cc.build_graph_db(GRAPH_ARCHIVE, GRAPH_DIR, sim_metric)

    def get_graph_labels(self):
        return k_s.RandomWalkKernelStrategy(cc.build_graph_db(GRAPH_ARCHIVE, GRAPH_DIR, RW_KERNEL)[0]).labels

if __name__ == '__main__':
    unittest.main()
