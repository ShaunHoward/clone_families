__author__ = 'shaun'

import random
import os
import unittest

import numpy as np

from similarity_strategy import similarity_strategies as sim
import clone_commander as cc
from test_tools import test_tools_utils as ttu

DIR = os.path.dirname(__file__)
TEST_TOOLS = os.path.abspath(os.path.join(DIR, '..', 'test_tools'))
TRAINING = os.path.abspath(os.path.join(DIR, '..', 'training_data'))
GRAPH_DIR = os.path.join(TRAINING, 'jgit_subset')
GRAPH_ARCHIVE = os.path.join(TRAINING, 'jgit_test.tar.gz')

graph_db = cc.build_graph_db(GRAPH_ARCHIVE, GRAPH_DIR, sim.LED)[0]


class TestLevenshteinStrategy(unittest.TestCase):

    def test_graph_comparison(self):
        # LED: returns integers; 0 is closest similarity
        led_context = sim.generate_strategy_context(sim.LED, graph_db)
        led_scores = sim.generate_similarity_scores(graph_db, led_context)
        eps = 0.01
        # test using scores from rw kernel, pick an arbitrary graph and divide its score list into
        # three categories; pick 1 other graph from each of the last 2 categories
        rw_scores = ttu.parse_scores(os.path.join(TEST_TOOLS, 'RW_KERNEL_SCORES.dat'))
        tested = set()
        for i in range(5):
            g = random.randint(0, len(rw_scores)-1)
            while g in tested:
                g = random.randint(0, len(rw_scores)-1)
            tested.add(g)

            bucket1, bucket2, bucket3, divisions = self.get_score_buckets(g, rw_scores)

            g1 = bucket1[0]  # least similar graph to 'g' of the 3
            g2 = bucket2[0]
            g3 = bucket3[0]  # most similar graph to 'g' of the 3

            if led_scores[g][g3] <= led_scores[g][g2] or led_scores[g][g2] <= led_scores[g][g1]:
                print 'WARNING: led scores mismatch rw scores'
                print 'bucket divisions:', divisions[0], divisions[1], divisions[2], divisions[3]
                print 'bucket lengths:', len(bucket1), ',', len(bucket2), ',', len(bucket3)
                print 'g1_g rw score:', rw_scores[g][g1], ', g2_g rw score:', rw_scores[g][g2], ', g3_g rw score:', rw_scores[g][g3]
                print 'g1_g LED score:', led_scores[g][g1], ', g2_g LED score:', led_scores[g][g2], ', g3_g LED score:', led_scores[g][g3]
                print 'g:', g, ', g1:', g1, ', g2:', g2, ', g3:', g3
                print ''
            else:
                self.assertLess(led_scores[g][g2], led_scores[g][g3] + eps)
                self.assertLess(led_scores[g][g1], led_scores[g][g2] + eps)

    def get_score_buckets(self, graph_num, scores):
        # returns sorted score buckets
        scores_graph_num = np.array(scores[graph_num])
        divisions = []
        min = scores_graph_num.min()
        max = scores_graph_num.max()
        div1 = min + ((max - min) * 0.5)
        div2 = min + ((max - min) * 0.75)
        divisions.append(min)
        divisions.append(div1)
        divisions.append(div2)
        divisions.append(max)
        bucket1 = []
        bucket2 = []
        bucket3 = []
        for i in range(1, len(scores_graph_num)):
            score = scores_graph_num[i]
            if score < div1:
                bucket1.append(i)
            elif div1 <= score < div2:
                bucket2.append(i)
            else:
                bucket3.append(i)
        bucket1.sort(reverse=True)  # bucket1[0] is greatest element
        bucket2.sort(reverse=True)
        bucket3.sort(reverse=True)
        return bucket1, bucket2, bucket3, divisions
