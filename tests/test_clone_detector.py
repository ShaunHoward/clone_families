__author__ = 'shaun'

import unittest

import os

import clone_commander as cc
from similarity_strategy.similarity_strategies import KMEANS, LED, RW_KERNEL
from clone_detector import clone_detector as cd

DIR = os.path.dirname(__file__)
TRAINING = os.path.abspath(os.path.join(DIR, '..', 'training_data'))
GRAPH_DIR = os.path.join(TRAINING, 'jgit_subset')
GRAPH_ARCHIVE = os.path.join(TRAINING, 'jgit_test.tar.gz')


class TestCloneDetector(unittest.TestCase):

    def test_group_by_families(self):
        """
        Tests that there is no more than 1 copy of each graph found
        in the transitive closure of all similar pairs.
        """
        curr_strategy = LED
        graph_db = self.build_graph_db(curr_strategy)
        clone_families = cd.run_clone_detection(graph_db[0], curr_strategy)
        # clone_families = cd.group_by_families(similar_pairs)

        # make sure num graphs found is either 0 or 1, no more
        num_graphs_found = []
        for i in range(len(graph_db[0])):
            num_graphs_found.append(0)
            for family in clone_families:
                if i in family:
                    num_graphs_found[i] += 1

        # assert that there is only at most one copy per graph
        for num_graphs in num_graphs_found:
            self.assertLessEqual(num_graphs, 1)

    def test_eliminate_redundant_clones(self):
        sorted_seeds = [1, 27, 32, 33, 34, 35, 37, 38, 46, 47,
                        49, 56, 64, 67, 73, 81]
        clone_families_in = {32: set([33, 34, 35, 37, 38, 49]),
                             1: set([2]),
                             34: set([49, 35, 52, 37, 38]),
                             35: set([49, 52, 37, 38]),
                             37: set([49, 52, 38]),
                             38: set([49, 52]),
                             33: set([34, 35, 37, 38, 49, 52]),
                             64: set([65, 66]),
                             73: set([74]),
                             46: set([48, 47]),
                             47: set([48]),
                             49: set([52]),
                             67: set([68]),
                             56: set([58]),
                             27: set([28]),
                             81: set([82])}
        clone_families_out = {32: set([33, 34, 35, 37, 38, 49, 52]),
                              1: set([2]),
                              34: set([]),
                              35: set([]),
                              37: set([]),
                              38: set([]),
                              33: set([]),
                              64: set([65, 66]),
                              73: set([74]),
                              46: set([48, 47]),
                              47: set([]),
                              49: set([]),
                              67: set([68]),
                              56: set([58]),
                              27: set([28]),
                              81: set([82])}

        # eliminate redundant clones transitive closure step
        clone_families_result, total_skipped, total_deleted =\
            cd.eliminate_redundant_clones(sorted_seeds, clone_families_in)

        # assure that we have not yet lost any clone families
        self.assertEqual(len(sorted_seeds), len(clone_families_result.keys()))

        # assert that the algorithm may have skipped clones
        self.assertGreaterEqual(total_skipped, 0)

        # assert the algorithm has deleted clones
        self. assertGreater(total_deleted, 0)

        # check that the result and actual output families are equal
        for family in clone_families_result.keys():
            # assert that the family from closure result is in the actual output family dict
            self.assertTrue(family in clone_families_out)
            # assert that the contents of the result family and the actual family are equal
            self.assertEqual(clone_families_result[family], clone_families_result[family])

    def test_transitive_closure_step(self):
        clone_families_in = {32: set([33, 34, 35, 37, 38, 49]),
                             1: set([2]),
                             34: set([49, 35, 52, 37, 38]),
                             35: set([49, 52, 37, 38]),
                             37: set([49, 52, 38]),
                             38: set([49, 52]),
                             33: set([34, 35, 37, 38, 49, 52]),
                             64: set([65, 66]),
                             73: set([74]),
                             46: set([48, 47]),
                             47: set([48]),
                             49: set([52]),
                             67: set([68]),
                             56: set([58]),
                             27: set([28]),
                             81: set([82])}
        clone_families_out = {32: set([33, 34, 35, 37, 38, 49, 52]),
                              1: set([2]),
                              34: set([49, 35, 52, 37, 38]),
                              35: set([49, 52, 37, 38]),
                              37: set([49, 52, 38]),
                              38: set([49, 52]),
                              33: set([]),
                              64: set([65, 66]),
                              73: set([74]),
                              46: set([48, 47]),
                              47: set([48]),
                              49: set([52]),
                              67: set([68]),
                              56: set([58]),
                              27: set([28]),
                              81: set([82])}
        i = 32
        j = 33
        clone_families_result, total_skipped, total_deleted = cd.transitive_closure_step(clone_families_in, i, j)

        # assure that we have not yet lost any clone families
        self.assertEqual(len(clone_families_in.keys()), len(clone_families_result.keys()))

        # assert that the algorithm skipped 1 clone
        self.assertEqual(total_skipped, 1)

        # assert the algorithm has deleted 6 clones
        self. assertEqual(total_deleted, 6)

        # check that the result and actual output families are equal
        for family in clone_families_result.keys():
            # assert that the family from closure result is in the actual output family dict
            self.assertTrue(family in clone_families_out)
            # assert that the contents of the result family and the actual family are equal
            self.assertEqual(clone_families_result[family], clone_families_result[family])

    def test_check_family_closure(self):
        clone_families_in = {32: set([33, 34, 35, 37, 38, 49, 52]),
                             1: set([2]),
                             64: set([65, 66]),
                             73: set([74]),
                             46: set([48, 47]),
                             67: set([68]),
                             56: set([58]),
                             27: set([28]),
                             81: set([82])}
        orig_num_clones = 38
        total_deleted = 22
        total_skipped = 1
        correct_closure = cd.check_family_closure(clone_families_in, orig_num_clones, total_deleted, total_skipped)
        self.assertTrue(correct_closure)

    def test_create_family_clusters(self):
        clone_families_in = {32: set([33, 34, 35, 37, 38, 49, 52]),
                             1: set([2]),
                             64: set([65, 66]),
                             73: set([74]),
                             46: set([48, 47]),
                             67: set([68]),
                             56: set([58]),
                             27: set([28]),
                             81: set([82])}
        clone_family_clusters_out = [[32, 33, 34, 35, 37, 38, 49, 52],
                                     [1, 2], [64, 65, 66], [73, 74],
                                     [46, 48, 47], [67, 68], [56, 58],
                                     [27, 28], [81, 82]]

        clone_family_clusters = cd.create_family_clusters(clone_families_in)

        # assert that the families are in a list
        self.assertEqual(type(clone_family_clusters), list)

        # assert at least one family is returned
        self.assertGreater(len(clone_family_clusters), 0)

        # assert families are lists
        self.assertEqual(type(clone_family_clusters[0]), list)

        # assert number of output clusters equal to actual
        self.assertEqual(len(clone_family_clusters_out), len(clone_family_clusters))

        for i in range(len(clone_family_clusters)):
            # assert non-empty family list
            self.assertGreater(len(clone_family_clusters[i]), 0)

            # assert that output family implies the actual output and vice-versa (transitive closure check)
            # if and only if logical implication to prove closure works
            self.assertTrue(clone_family_clusters_out[i] in clone_family_clusters)
            self.assertTrue(clone_family_clusters[i] in clone_family_clusters_out)

    def test_remove_empty_families(self):
        # test empty at beginning, middle, and end
        clone_fam_w_end_empty = {1: [2], 2: [3, 4], 3: []}
        clone_fam_w_mid_empty = {1: [2], 2: [], 3: [4, 5]}
        clone_fam_w_front_empty = {1: [], 2: [3, 4], 3: [4, 5]}

        fam_dict_list = [clone_fam_w_end_empty, clone_fam_w_mid_empty, clone_fam_w_front_empty]
        for fam_dict in fam_dict_list:
            clone_fam_result = cd.remove_empty_families(fam_dict)
            self.assertEqual(len(clone_fam_result), 2)
            for fam in clone_fam_result.keys():
                self.assertIsNotNone(clone_fam_result[fam])
                self.assertEqual(type(clone_fam_result[fam]), list)
                self.assertGreater(clone_fam_result[fam], 0)

    def test_find_clone_families_levenshtein_time(self):
        # test graph sim with timing
        graph_db = self.build_graph_db(LED)
        clone_families, time = cd.find_clone_families(graph_db[0], metric_type=LED, is_timed=True)

        # assert clone families are list and non-empty
        self.assertEqual(type(clone_families), list)
        self.assertGreater(len(clone_families), 0)

        # assert time is a float and is > 0
        self.assertEqual(type(time), float)
        self.assertGreater(time, 0)

        # test graph sim w.o timing
        clone_families, time = cd.find_clone_families(graph_db[0], metric_type=LED, is_timed=False)

        # assert clone families are list and non-empty
        self.assertEqual(type(clone_families), list)
        self.assertGreater(len(clone_families), 0)

        # assert time is 0 when not timed
        self.assertEqual(time, 0)

    def test_find_clone_families_kernel(self):
        # test kernel method (diff graph db than LED)
        graph_db = self.build_graph_db(RW_KERNEL)
        clone_families, time = cd.find_clone_families(graph_db[0], metric_type=RW_KERNEL, is_timed=False)

        # assert clone families are list and non-empty
        self.assertEqual(type(clone_families), list)
        self.assertGreater(len(clone_families), 0)

        # assert time is 0 when not timed
        self.assertEqual(time, 0)

    def test_run_clone_detection_graph_similarity(self):
        """
        Test the code clone detection method with graph similarity
        methods like Levenshtein and Kernel.
        """
        curr_strategy = LED
        graph_db = self.build_graph_db(curr_strategy)
        similar_pairs = cd.run_clone_detection(graph_db[0], curr_strategy)

        # assert clone families are in non-empty list
        self.assertIsNotNone(similar_pairs)
        self.assertEqual(type(similar_pairs), list)
        self.assertGreater(len(similar_pairs), 0)

        # assert similar pairs is a list and is non-empty
        for sim_pair in similar_pairs:
            # assert sim pair is a tuple
            self.assertEqual(type(sim_pair), list)
            self.assertGreaterEqual(len(sim_pair), 2)

            # item is a 2-tuple of similar graph indices
            self.assertEqual(type(sim_pair[0]), int)
            self.assertEqual(type(sim_pair[1]), int)

    def test_run_clone_detection_kmeans_clustering(self):
        """
        Test code clone detection method with k-means clustering
        method.
        """
        curr_strategy = KMEANS
        graph_db = self.build_graph_db(curr_strategy)
        clone_families = cd.run_clone_detection(graph_db[0], curr_strategy)

        # assert clone families are in non-empty list
        self.assertIsNotNone(clone_families)
        self.assertEqual(type(clone_families), list)
        self.assertGreater(len(clone_families), 0)

        # assert family is a list and is non-empty
        for family in clone_families:
            self.assertEqual(type(family), list)
            self.assertGreater(len(family), 0)
            self.assertEqual(type(family[0]), int)

    def build_graph_db(self, sim_metric):
        return cc.build_graph_db(GRAPH_ARCHIVE, GRAPH_DIR, sim_metric)


if __name__ == '__main__':
    unittest.main()
