from utilities import io_utils as io
import os
from os import path
import unittest

DIR = os.path.dirname(__file__)
TRAINING = os.path.abspath(os.path.join(DIR, '..', 'training_data'))
GRAPH_DIR = os.path.join(TRAINING, 'jgit_subset')
GRAPH_ARCHIVE = os.path.join(TRAINING, 'jgit_test.tar.gz')
TEST_UNARCHIVE_DIR = os.path.join(TRAINING, 'test_unarchive')


class TestIOUtils(unittest.TestCase):

    def assert_is_labeled_list(self, labeled_list):
        self.assertEqual(type(labeled_list), tuple)
        self.assertEqual(len(labeled_list), 2)
        self.assertEqual(type(labeled_list[0]), list)
        self.assertEqual(type(labeled_list[1]), str)

    def test_untar_file(self):
        io.delete_if_exists(TEST_UNARCHIVE_DIR)  # implicit test for this method
        io.untar_file(GRAPH_ARCHIVE, TEST_UNARCHIVE_DIR)
        self.assertTrue(path.exists(TEST_UNARCHIVE_DIR))
        io.delete_if_exists(TEST_UNARCHIVE_DIR)

    def test_read_file_contents(self):
        file_path = GRAPH_DIR + '/0/pattern.dot'
        charcount = 0
        f = open(file_path, 'r')
        content_list = []
        for line in f:
            content_list.append(line)
            charcount += len(line)
        f.seek(0)

        self.assertEqual(len(content_list), len(f.readlines()))
        j = '\n'.join(content_list)
        self.assertEqual(len(j), charcount + len(content_list) - 1)

    def test_get_dot_file_paths(self):
        file_name = 'pattern.dot'
        directory = GRAPH_DIR
        file_paths = io.get_dot_file_paths(directory, file_name)
        self.assert_is_labeled_list(file_paths)
        if directory.endswith('subset'):
            self.assertEqual(len(file_paths[0]), 100)
        elif directory.endswith('small'):
            self.assertEqual(len(file_paths[0]), 16)
        elif directory.endswith('test'):
            self.assertEqual(len(file_paths[0]), 11)
        elif directory.endswith('jgit'):
            self.assertEqual(len(file_paths[0]), 1971)
        else:
            print 'test_get_dot_file_paths: unsupported test directory'
            self.assertTrue(True)

if __name__ == '__main__': unittest.main()
