import os
from utilities import graph_utils as gu
from utilities import io_utils as io
from dot_tools.dot_graph import SimpleGraph
import unittest

DIR = os.path.dirname(__file__)
TRAINING = os.path.abspath(os.path.join(DIR, '..', 'training_data'))
GRAPH_DIR = os.path.join(TRAINING, 'jgit_subset')
GRAPH_ARCHIVE = os.path.join(TRAINING, 'jgit_test.tar.gz')

NumEmbeddedGraphsInGraphDir = 561

dot_str = io.read_file_contents(GRAPH_DIR + '/0/pattern.dot')
SimpleGraphInstance = gu.viz2simple_graph(dot_str)

SimpleGraphInstance_NumNodes = len(SimpleGraphInstance.nodes)  # 9
SimpleGraphInstance_NumEdges = len(SimpleGraphInstance.edges)  # 8


class TestGraphUtils(unittest.TestCase):

    def assert_is_labeled_list(self, labeled_list):
        self.assertEqual(type(labeled_list), tuple)
        self.assertEqual(len(labeled_list), 2)
        self.assertEqual(type(labeled_list[0]), list)
        self.assertEqual(type(labeled_list[1]), str)

    def assert_is_triple_graph(self, graph):
        self.assertEqual(type(graph), list)
        self.assertGreater(len(graph), 0)
        self.assertEqual(type(graph[0]), tuple)

    def assert_is_simple_graph(self, graph):
        self.assertEqual(type(graph), SimpleGraph)
        self.assertGreater(len(graph.nodes), 0)
        self.assertGreater(len(graph.edges), 0)

    def assert_is_graph_db(self, graph_db, check_graph_method):
        self.assertEqual(type(graph_db), type(()))  # tuple
        self.assertEqual(len(graph_db), 2)
        self.assertEqual(type(graph_db[0]), type([]))  # list
        self.assertGreater(len(graph_db[0]), 0)
        for graph in graph_db[0]:
            check_graph_method(graph)
        self.assertEqual(type(graph_db[1]), type(" "))

    def assert_num_graphs_equal(self, list, directory):
        if directory.endswith('subset'):
            self.assertEqual(len(list), 100)
        elif directory.endswith('small'):
            self.assertEqual(len(list), 16)
        elif directory.endswith('test'):
            self.assertEqual(len(list), 11)
        elif directory.endswith('jgit'):
            self.assertEqual(len(list), 1971)
        else:
            self.assertTrue(True) # unsupported graph directory

    def test_viz2simple_graph(self):
        self.assert_is_simple_graph(SimpleGraphInstance)

    def test_parse_simple_graph(self):
        self.assert_is_simple_graph(gu.parse_simple_graph(dot_str))

    def test_create_graph_db(self):
        # test triple graph graph_db
        graph_db = gu.create_graph_db(GRAPH_ARCHIVE, GRAPH_DIR, True)
        self.assert_is_graph_db(graph_db, self.assert_is_triple_graph)
        self.assert_num_graphs_equal(graph_db[0], GRAPH_DIR)
        # test simple graph graph_db
        graph_db = gu.create_graph_db(GRAPH_ARCHIVE, GRAPH_DIR, False)
        self.assert_is_graph_db(graph_db, self.assert_is_simple_graph)
        self.assert_num_graphs_equal(graph_db[0], GRAPH_DIR)

    def test_extract_triples_from_graph(self):
        triple_list = gu.extract_triples_from_graph(SimpleGraphInstance)
        self.assert_is_triple_graph(triple_list)
        self.assertEquals(len(triple_list), SimpleGraphInstance_NumEdges)

    def test_read_pattern_graphs(self):
        # test triple graphs
        graphs_triples = gu.read_pattern_graphs(GRAPH_DIR, True)
        self.assert_is_labeled_list(graphs_triples)
        self.assert_num_graphs_equal(graphs_triples[0], GRAPH_DIR)
        for graph in graphs_triples[0]:
            self.assert_is_triple_graph(graph)
        # test simple graphs
        graphs_not_triples = gu.read_pattern_graphs(GRAPH_DIR, False)
        self.assert_is_labeled_list(graphs_not_triples)
        self.assert_num_graphs_equal(graphs_not_triples[0], GRAPH_DIR)
        for graph in graphs_not_triples[0]:
            self.assert_is_simple_graph(graph)

    def test_build_graph_hash_table(self):
        pass

    def test_get_pattern_graph_path(self):
        path = gu.get_pattern_graph_path(0, GRAPH_DIR)
        self.assertTrue(path.__contains__("/projects/eecs493/clone_families/training_data/jgit_subset/0/pattern.dot"))

    def test_get_embedded_graph_paths(self):
        embedded_paths = gu.get_embedded_graph_paths(0, GRAPH_DIR)
        self.assertEqual(type(embedded_paths), type([]))
        self.assertEqual(len(embedded_paths), 5)  # number of embedded graphs for pattern 0
        self.assertTrue(embedded_paths.__contains__(GRAPH_DIR + '/0/instances/0/embedding.dot'))

    def test_triples(self):
        triples_ = gu.triples(SimpleGraphInstance)
        self.assert_is_triple_graph(triples_)
        self.assertEqual(len(triples_), SimpleGraphInstance_NumEdges)
        # test ordering of triples as defined by Bliss algorithm TODO

    def test_get_graph_file_paths(self):
        paths_in_dir = gu.get_graph_file_paths(GRAPH_DIR, 'pattern.dot')
        self.assert_is_labeled_list(paths_in_dir)
        self.assert_num_graphs_equal(paths_in_dir[0], GRAPH_DIR)

    def test_get_adjacency_matrix(self):
        adjacency = gu.get_adjacency_matrix(SimpleGraphInstance.nodes, SimpleGraphInstance.edges)
        self.assertEqual(adjacency.sum(), SimpleGraphInstance_NumEdges)  # not symmetric


if __name__ == '__main__': unittest.main()
