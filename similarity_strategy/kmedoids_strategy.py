__author__ = 'shaun howard'
from Levenshtein import hamming, distance
from utilities import graph_utils
from random import randint


def compute_kmedoids_clusters(graph_db, MAX_ITERS=3):
    """
    Creates gled k-medoids clusters
    based on the graph Levenshstein edit distance
    and seeded by a novel heuristic distance
    using the hamming string similarity metric.
    The number of clusters is heuristically
    determined.
    :param graph_db: the database of frequent subgraphs
    :return: the clusters of similar frequent subgraphs
    """
    # gather all graph string codes
    bsde_codes = [graph_utils.graph_2_string(graph) for graph in graph_db]
    # seed the initial clusters
    seeded_clusters = seed_clusters(bsde_codes)

    # get the list of clusters
    clusters = seeded_clusters.values()

    changes = 1
    curr_iter = 0
    # perform random sampling k-medoids clustering
    while changes > 0 and curr_iter < MAX_ITERS:
        print "clustering iteration: %d" % curr_iter
        num_changes = 0
        # go through each graph string and try to assign it to a cluster
        # remove it from all clusters if it a new one cannot be assigned
        # when one can be assigned, remove it from its old one and assign it the new one
        for curr_code_index in range(len(bsde_codes)):
            # try to find a new cluster for the current data point
            new_cluster_index = find_new_cluster_or_ditch(bsde_codes[curr_code_index], clusters, bsde_codes)

            # remove the data point from any clusters if it cannot be assigned to a new cluster
            # during this iteration
            clusters = remove_from_cluster(curr_code_index, clusters, new_cluster_index)

            # remove any empty clusters that may have arisen from data point removal
            clusters = remove_empty_clusters(clusters)

            # when a new cluster is found, remove the data point from its old one and assign it the new one
            # permitting the one found is not the same one as it is currently assigned
            if new_cluster_index > -1:
                # remove data point from its old cluster and get the old cluster index
                cluster_found_in = find_curr_cluster(curr_code_index, clusters)
                # continue if no changes to cluster for this data point
                if cluster_found_in == new_cluster_index:
                    continue
                elif cluster_found_in > -1:
                    # otherwise remove the data point from its old cluster
                    clusters[cluster_found_in].remove(curr_code_index)
                # add data point to its new cluster
                clusters[new_cluster_index].append(curr_code_index)
                num_changes += 1
        changes = num_changes
        curr_iter += 1
    graph_utils.show_clusters(clusters, bsde_codes, 'K-medoids')
    return clusters


def find_curr_cluster(curr_code_index, clusters):
    """
    Find the current cluster of the provided graph code
    from the list of clusters. Return the index of the
    cluster in which it is found.
    :param curr_code_index: the current graph string code index
    :param clusters: the list of clusters currently available
    :return: the index of the cluster the current code is found in
    or -1 if none
    """
    cluster_found_in = -1
    # remove the code from its old cluster
    for j in range(len(clusters)):
        # get cluster this code is in and break
        if curr_code_index in clusters[j]:
            cluster_found_in = j
            break
    return cluster_found_in


def remove_from_cluster(curr_code_index, clusters, cluster_index):
    """
    Removes the current graph string code from the
    cluster at the specified cluster index unless it is -1,
    in which nothing is removed.
    :param curr_code_index: the index of the current graph string code
    :param clusters: the list of clusters
    :param cluster_index: the cluster index to remove the curr code index from
    :return: the list of clusters with the curr code index removed unless
    cluster index is -1, in which nothing is removed
    """
    if cluster_index == -1:
        # remove the code from its old cluster
        for cluster in clusters:
            if curr_code_index in cluster:
                cluster.remove(curr_code_index)
    return clusters


def remove_empty_clusters(clusters):
    """
    Removes any empty clusters from the list
    of clusters
    :param clusters: list of clusters
    :return: list of clusters with no empty clusters
    """
    return [x for x in clusters if x]


def find_new_cluster_or_ditch(curr_bsde_code, clusters, bsde_codes):
    """
    The medoid comparison step of k-medoids algorithm.
    Randomly samples a medoid from each cluster and compares
    the current bsde graph code to each one. The minimum one that
    has an edit distance score of less than or equal to the length of the current
    bsde code is where the curr graph code is assigned.
    :param curr_bsde_code: the current string bsde graph code
    :param clusters: the list of clusters
    :param bsde_codes: the list of graph bsde codes available
    :return: the cluster index of the assigned cluster or -1
    if no cluster was found
    """
    cluster_index = -1
    scores = []
    for cluster in clusters:
        # randomly pick a representative graph in the cluster to compare to
        rep_index = randint(0, len(cluster)-1)
        rep_code = bsde_codes[cluster[rep_index]]
        # find the levenshtein distance between the current and representative codes
        scores.append(distance(curr_bsde_code, rep_code))
    min_score = min(scores)
    # only use a cluster that is similar to at least half of the graph
    if min_score <= len(curr_bsde_code) / 2:
        cluster_index = scores.index(min_score)
    return cluster_index


def seed_clusters(bsde_codes):
    """
    Seeds clusters using the frequent hamming distances
    between each graphs bsde code and a string of "."s
    the same length as the current bsde code.
    :param bsde_codes: the database of bsde_codes for subgraphs
    :return: the seeds of the k-means clusters for the
    provided bsde_codes
    """
    score_list = []
    for code in bsde_codes:
        # use the hamming distance between this string a string of all .'s of same length
        score_list.append(len(code)*hamming(code, '.'*len(code)))

    frequent_values = get_frequent_values(score_list)

    # keyed by score, value is list of similar graphs
    seeded_clusters = {val: [] for val in frequent_values}
    for seed_val in seeded_clusters.keys():
        seeded_clusters[seed_val] = [i for i, score in enumerate(score_list) if score == seed_val]
    return seeded_clusters


def get_frequent_values(scores):
    """
    Finds the a list of frequent score values
    that come up in the provided scores list
    :param scores: a list of scores
    :return: a list of scores that occur more than once in
    the provided scores list
    """
    score_buckets = {}
    for score in scores:
        if score not in score_buckets.keys():
            score_buckets[score] = 1
        else:
            score_buckets[score] += 1
    frequent_vals = []
    for score in score_buckets.keys():
        if score_buckets[score] > 1:
            frequent_vals.append(score)
    return frequent_vals
