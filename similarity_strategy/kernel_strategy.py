__author__  =  'Ben Marks, Shaun Howard'

import math
import numpy as np
import itertools as it
from utilities.graph_utils import get_adjacency_matrix

DEFAULT_ISO_VERTICES = 4


class RandomWalkKernelStrategy():

    labels = []

    def __init__(self, graph_db):
        self.labels = get_labels(graph_db)

    def graph_comparison(self, graph_db):
        """
        Run the random walk kernel algorithm on the provided sub-graph database
        to determine how closely the graphs are related.
        :param graph_db: the database of graphs to run graph comparison on
        :return: the random walk kernel score matrix between the graphs provided
        """
        lal_matrices = []
        scores = np.zeros((len(graph_db), len(graph_db)))

        # create all label adjacency label matrices
        for i, graph in enumerate(graph_db):
            lal_matrices.append(get_label_adjacency_label(graph, self.labels))

        # determine all scores and reflect them due to symmetry
        for i in range(len(graph_db)):
            for j in range(i+1, len(graph_db)):
                scores[i][j] = get_random_walk_kernel_scores(lal_matrices[i], lal_matrices[j])
                scores[j][i] = scores[i][j]  # because of symmetry

        # eliminate cyclic graph scores
        for i in range(len(graph_db)):
            if np.linalg.norm(lal_matrices[i]) == 0:
                for j in range(len(graph_db)):
                    scores[i][j] = scores.max()
                    scores[j][i] = scores[i][j]
        return scores


def isomorphic_kernel_graph_comparison(graph_db, num_sub_graph_vertices=DEFAULT_ISO_VERTICES):
    """
    Run the isomorphic kernel algorithm on the provided input sub-graph database
    to determine how closely the graphs are related.
    :param graph_db: the database of graphs to run graph comparison on
    :param num_sub_graph_vertices: the number of vertices to use in the kernel
    :return: the isomorphic kernel scores matrix between the graphs provided
    """
    subgraphs = []
    scores = np.zeros((len(graph_db), len(graph_db)))
    for i, graph in enumerate(graph_db):
        subgraphs.append(get_sub_graphs(graph, num_sub_graph_vertices))

    for i in range(len(graph_db)):
        for j in range(i+1):
            scores[i][j] = get_isomorphic_kernel_scores(subgraphs[i], subgraphs[j])
            scores[j][i] = scores[i][j]  # because of symmetry
    return scores


def get_labels(graph_db):
    """
    Creates a set of labels that occur in each graph's nodes in graph_db and then converts it to a list.
    Used in the random walk kernel.
    :param graph_db: a list of graphs to be compared
    :return: a list containing each label from every graph's nodes in graph_db such that each label occurs only
    once in the list
    """
    labels = set()
    for graph in graph_db:
        for node in graph.nodes.values():
            labels.add(node["label"])
    return list(labels)


def get_label_adjacency_label(graph, labels):
    """
    Creates the label adjacency label matrix for this graph. First it gets the label matrix. Then it gets the adjacency
    matrix. Then it continually adds powers of the adjacency matrix (i.e. can be reached in i edges) to get the total
    number of paths from a node to each other node. Used in random walk kernel.
    :param graph: a set of labeled nodes and edges
    :param labels: the list of all possible labels that could occur in this graph
    :return: a matrix representing the relative frequencies of the labels scaled by the sum of powers of the adjacency
    matrix
    """
    label_matrix = get_label_matrix(graph.nodes, labels)
    adjacency = get_adjacency_matrix(graph.nodes, graph.edges)
    adjacency_product = np.identity(len(graph.nodes), dtype=np.int64)
    adjacency_sum = np.zeros((len(graph.nodes), len(graph.nodes)), dtype=np.int64)
    for i in range(len(graph.nodes)):
        adjacency_product = adjacency_product.dot(adjacency)
        adjacency_sum += adjacency_product

    # make graphs that have cycles all zeroes
    for i in range(len(graph.nodes)):
        if adjacency_sum[i][i] > 0:
            return np.zeros((len(labels), len(labels)))

    return label_matrix.dot(adjacency_sum).dot(label_matrix.transpose())


def get_label_matrix(nodes, labels):
    """
    Creates a matrix that represents the mapping of labels to the nodes that have them. Used in random walk kernel.
    :param nodes: a list of the labeled nodes of a graph (size N)
    :param labels: the list of all possible labels that could occur in the list of nodes (size L)
    :return: a L by N matrix M with Mij=1 if the label of node j is label i and 0 otherwise.
    """
    node_keys = nodes.keys()
    label_matrix = np.zeros((len(labels), len(nodes)), dtype=np.int64)
    for i in range(len(labels)):
        for j in range(len(nodes)):
            label_matrix[i][j] = labels[i] == nodes[node_keys[j]]["label"]
    return label_matrix


def get_random_walk_kernel_scores(g1_label_adj_label, g2_label_adj_label):
    """
    Returns the score of distance between two graphs based on the difference between their label adjacency label
    matrices.
    :param g1_label_adj_label: a label adjacency label matrix
    :param g2_label_adj_label: another label adjacency label matrix
    :return: the norm of the difference between the two matrices
    """
    return np.linalg.norm(g1_label_adj_label-g2_label_adj_label)


def get_isomorphic_kernel_scores(sub_graphs_1, sub_graphs_2):
    """
    Returns the score of the isomorphic kernel by looking at the difference in counts of each type of subgraph. It uses
    a exponential kernel with sigma equal to the sqrt(0.5).
    :param sub_graphs_1: the list of subgraph counts from one graph
    :param sub_graphs_2: the list of subgraph counts from another graph
    :return: the exponential kernel of the difference in counts of each type of subgraph
    """
    score = 0

    s1_unused = range(len(sub_graphs_1))
    s2_unused = range(len(sub_graphs_2))
    # Use the exponential kernel
    for s1 in sub_graphs_1:
        for s2 in sub_graphs_2:
            if is_isomorphic(s1["subgraph"], s2["subgraph"]):
                try:
                    s1_unused.remove(sub_graphs_1.index(s1))
                except ValueError:
                    pass
                try:
                    s2_unused.remove(sub_graphs_2.index(s2))
                except ValueError:
                    pass
                score += (s1["count"] - s2["count"])**2
                break

    # Add in unmatched counts
    for i in s1_unused:
        score += sub_graphs_1[i]["count"]**2

    for i in s2_unused:
        score += sub_graphs_2[i]["count"]**2

    # Should be (1.0/math.sqrt(math.pi)**N), where N is the maximal number of subgraphs that can be mutually isomorphic
    # to each other, but that is both hard to calculate and unnecessary because of further normalization and the ability
    # to set the epsilon value later to determine similarity
    return math.exp(-score)


def get_sub_graphs(graph, num_vertices_in_sub_graphs=3):
    """
    Gets the counts of each type of subgraph for every combination of num_vertices_in_sub_graphs vertices in the graph.
    :param graph: a list of vertices and edges of the graph
    :param num_vertices_in_sub_graphs: the number of vertices to use in each subgraph (defaults to 3)
    :return: a list of the counts of each type of subgraph that occur in each combination of num_vertices_in_sub_graphs
    vertices in graph
    """
    vertices = graph.nodes
    combinations = it.combinations(vertices, num_vertices_in_sub_graphs)
    sub_graph_counts = []
    num_graphlets = 0
    for a in combinations:
        sub_graph = dict()
        sub_graph["edges"] = []
        # node supplies are the number of outgoing edges minus the number of incoming edges
        sub_graph["node_supplies"] = [0 for _ in range(len(a))]
        sub_graph["vertices"] = a
        for edge in graph.edges:
            if edge[0] in a and edge[1] in a:
                sub_graph["edges"].append(edge)
                sub_graph["node_supplies"][a.index(edge[0])] += 1
                sub_graph["node_supplies"][a.index(edge[1])] -= 1
        found = 0
        sub_graph["node_supplies"].sort()
        for s in sub_graph_counts:
            if is_isomorphic(sub_graph, s["subgraph"]):
                found = 1
                s["count"] += 1
                break
        if not found:
            s = dict()
            s["subgraph"] = sub_graph
            s["count"] = 1
            sub_graph_counts.append(s)
        num_graphlets += 1
    # normalize to fractions of counts instead of true counts
    for i in range(len(sub_graph_counts)):
        sub_graph_counts[i]["count"] = 1.0 * sub_graph_counts[i]["count"] / num_graphlets

    return sub_graph_counts


def is_isomorphic(graph1, graph2):
    """
    Determines if two subgraphs are probably isomorphic to each other with respect to the structure
    :param graph1: a list of edges and sorted node supplies (defined above)
    :param graph2: another list of edges and sorted node supplies (defined above)
    :return: True if the graphs are probably isomorphic to each other with respect to the structure and False otherwise
    """
    # if they don't have the same number of edges, they can't be isomorphic
    if len(graph1["edges"]) != len(graph2["edges"]):
        return False
    # if the node supplies (which are both sorted) are different, they can't be isomorphic
    for i in range(len(graph1["node_supplies"])):
        if graph1["node_supplies"][i] != graph2["node_supplies"][i]:
            return False
    # works for num_vertices_in_graph == 3, not sure about bigger subgraphs
    return True
