__author__ = 'shaun'

from Levenshtein.StringMatcher import distance


def graph_comparison(g1, g2):
    """
    Compares the bliss source destination edge code of
    the two input simple graphs with the Levenshtein
    edit distance.
    :param g1: graph 1 for comparison
    :param g2: graph 2 for comparison
    :return: the distance between graphs (0 is most similar)
    """
    g1_joins = []
    g2_joins = []

    # make the triples for each graph into strings
    for g in g1:
        g1_joins.append(''.join(g))
    for g in g2:
        g2_joins.append(''.join(g))

    # sort triple joins by source (first in triple) to make sure comparison is consistent
    g1_joins = sorted(g1_joins)
    g2_joins = sorted(g2_joins)

    # join all of the sorted triples into strings, each string representing
    # each graph to obtain the bliss source destination edge code for graphs 1 and 2
    bsde_code_1 = ''.join(g1_joins)
    bsde_code_2 = ''.join(g2_joins)

    # find the distance between the bsde codes of graphs 1 and 2
    bsde_code_dist = distance(bsde_code_1, bsde_code_2)

    return bsde_code_dist