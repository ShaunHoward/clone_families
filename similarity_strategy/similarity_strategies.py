
__author__ = 'Shaun Howard'

import numpy as np
import levenshstein_strategy as ls
import kernel_strategy as ks
from kernel_strategy import RandomWalkKernelStrategy as rwks

ISO_KERNEL = "isomorphic kernel"
RW_KERNEL = "random-walk kernel"
LED = "levenshtein"
KMEANS = "kmeans"
KMEDOIDS = "kmedoids"

COMPLEMENT_SCORES = "complement_scores"
EPSILON = "epsilon"
SIM_METHOD = "similarity_method"
GRAPH_STYLE = "graph_style"
NUM_CLUSTERS = "num_clusters"
INIT_TYPE = "init_type"
KMEANSPLUS = "k-means++"
RANDOM = "random"
KERNEL = "kernel"

NORMALIZE = "normalize"

SIMPLE_GRAPH = "simple_graph"
TRIPLES = "triples"


def jaccard_similarity_matrix(a, b):
    """
    Computes and returns the Jaccard
    similarity value between the two
    provided triple list graphs.
    :param a: a triple graph
    :param b: a triple graph
    :return: float [0, 1]
    """
    a = set(a)
    b = set(b)
    intersect = len(a & b)
    union = len(a) + len(b) - intersect
    return float(intersect)/float(union)


def multiply_scores(matrix, amount):
    """
    Multiply the matrix by the given amount and return it.
    """
    matrix_len = len(matrix)
    for i in range(matrix_len):
        for j in range(matrix_len):
            matrix[i, j] *= amount
    return matrix


def generate_strategy_context(similarity_strategy, graph_db):
    """
    Generate a strategy context for running the similarity algorithm
    with different similarity strategies.
    Set the epsilon thresholds, the method that will compare
    two sub-graphs and whether the scores need to be complemented.
    :param similarity_strategy: the similarity strategy to generate
    a context for based on the global list of similarity strategies
    :param graph_db: used to instantiate a random-walk kernel strategy
    if used
    :return: a context of similarity method, epsilon of tolerance
    in similarity measurement, and whether scores need to be complemented
    based on use of similarity/dissimilarity
    """
    context = dict()
    if similarity_strategy == LED:
        context[SIM_METHOD] = ls.graph_comparison
        context[COMPLEMENT_SCORES] = True
        context[EPSILON] = 0.01
        context[GRAPH_STYLE] = TRIPLES
        context[KERNEL] = 0
        context[NORMALIZE] = 1
    elif similarity_strategy == KMEANS:
        context[COMPLEMENT_SCORES] = True
        context[EPSILON] = 0.05
        context[GRAPH_STYLE] = SIMPLE_GRAPH
        context[NUM_CLUSTERS] = 20
        context[INIT_TYPE] = KMEANSPLUS
        context[KERNEL] = 0
        context[NORMALIZE] = 1
    elif similarity_strategy == ISO_KERNEL:
        context[SIM_METHOD] = ks.isomorphic_kernel_graph_comparison
        context[COMPLEMENT_SCORES] = False
        context[EPSILON] = 0.00005
        context[GRAPH_STYLE] = SIMPLE_GRAPH
        context[KERNEL] = 1
        context[NORMALIZE] = 0
    elif similarity_strategy == RW_KERNEL:
        rwk = rwks(graph_db)
        context[SIM_METHOD] = rwk.graph_comparison
        context[COMPLEMENT_SCORES] = True
        context[EPSILON] = 0.1
        context[GRAPH_STYLE] = SIMPLE_GRAPH
        context[KERNEL] = 1
        context[NORMALIZE] = 1
    return context


def generate_similarity_scores(graph_db, similarity_context):
    """
    Produces a similarity matrix from the i'th graph in the graph db to the j'th graph
    in the graph db with the method provided, which must accept two graphs as input and produce
    a score as output. A graph can have multiple representations. Levenshtein uses a list of triple lists representing
     one graph per row. Kernel uses a simple graph representation.

    The score is stored in a normalized fashion. The scores represent the likelihood
    that any two graphs are similar in structure and attributes based on the graph comparison method provided
    for comparing any two sub-graphs. A correct likelihood is only produced if the scores are stored in the
    correct representation.

    The most similar scores (when sub-graphs are isomorphic) will be 1. The least
    similar scores will be 0. Therefore, the similarity score must be represented in a range from 0 to 1
    where 0 is the least similar and 1 is the most similar. Hence, if the similarity metric used
    is really a dissimilarity metric, the data will need to be complemented so that output of 0
    represents most similar sub-graphs with a similarity score of 1.
    """

    if not similarity_context[KERNEL]:
        scores = np.zeros((len(graph_db), len(graph_db)))
        for i, a in enumerate(graph_db):
            for j, b in enumerate(graph_db):
                # specifically optimized to only compute unknown scores
                if i < j:
                    scores[i, j] = similarity_context[SIM_METHOD](a, b)
                elif i == j and not similarity_context[COMPLEMENT_SCORES]:
                    scores[i, j] = 1
                else:
                    scores[i, j] = scores[j, i]  # due to symmetry

    else:
        scores = similarity_context[SIM_METHOD](graph_db)

    # normalize scores if required by scoring metric
    if similarity_context[NORMALIZE]:
        scores = normalize(scores)

    if similarity_context[COMPLEMENT_SCORES]:
        for i in range(len(graph_db)):
            for j in range(len(graph_db)):
                # complement scores if required by scoring metric
                scores[i, j] = 1 - scores[i, j]
    return scores


def normalize(scores, column_wise=False):
    """
    Normalizes the score matrix to range [0, 1] using its min and max values.
    :param scores: the matrix of scores to normalize
    :param column_wise: whether or not to normalize data by column or entire patterns
    :return: the normalized score matrix
    """
    if column_wise:
        for j in range(scores.shape[1]):
            min_val = np.min(scores[:, j])
            max_val = np.max(scores[:, j])
            # attempt to normalize the scores produced by the comparison method
            if max_val - min_val != 0:
                scores[:, j] = (scores[:, j]-min_val)/(max_val-min_val)
    else:
        min_val = scores.min()
        max_val = scores.max()
        # attempt to normalize the scores produced by the comparison method
        if max_val - min_val != 0:
            for i in range(len(scores)):
                for j in range(len(scores[i])):
                    scores[i][j] = (scores[i][j]-min_val)/(max_val-min_val)

    return scores


def graph_similarity(graph_db, strategy_context):
    """
    Returns the list of similar sub-graphs according to the
    provided similarity context. A similarity context
    must include the method to call to find a similarity matrix of scores,
    whether to complement these scores during calculations, the epsilon
    of error for similarity threshold, and the graph representation style.

    After calculating the strategy-specific similarity or dissimilarity scores,
    this method calculates the true similarity between graphs by taking
    the normalized complement of the calculated scores between
    each of the sub-graphs in the graph db. Hence, it truly represents
    the likelihood that any two sub-graphs are similar to the order
    of epsilon that the context specifies.

    :param graph_db: is a list the graph representations necessary for the provided
    similarity context
    :param strategy_context: a similarity strategy context which includes different
    aspects of the similarity strategy desired to use for sub-graph comparison

    :return: a list of similar graph tuples (graphs represented as indices)
    """

    # create the similarity matrix
    scores = generate_similarity_scores(graph_db, strategy_context)
    similar_graphs = []

    # the epsilon threshold of similarity between two sub-graphs
    epsilon = strategy_context[EPSILON]
    for i in range(len(graph_db)):
        for j in range(len(graph_db)):
            # only track graphs that are more than the specified threshold similar
            if i < j and scores[i, j] > 1 - epsilon:
                similar_graphs.append((i, j))

    return similar_graphs
