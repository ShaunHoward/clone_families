__author__ = 'shaun'

from matplotlib import pyplot
import numpy as np
from sklearn.utils.graph_shortest_path import graph_shortest_path
from sklearn.cluster import KMeans
from similarity_strategies import EPSILON, INIT_TYPE, NUM_CLUSTERS, normalize
from utilities.graph_utils import get_adjacency_matrix
from Levenshtein import distance as leven_dist
from utilities import graph_utils

NUM_VERTICES = 'num_vertices'
NUM_EDGES = 'num_edges'
V_DEGREE = 'vertex_degree'
MIN_OUT_DEGREE = 'min_out_degree'
MAX_OUT_DEGREE = 'max_out_degree'
MIN_IN_DEGREE = 'min_in_degree'
MAX_IN_DEGREE = 'max_in_degree'
CLOSENESS_CENT = 'closeness_centrality'
DIAMETER = "diameter"
DENSITY = "density"
NUM_SHORTEST_PATH = "num_shortest_path"
DISCONNECTED_PAIRS = "disconnected_node_pair_count"
RATIO_CONN_DISCON = "ratio_connected_disconnected"
RATIO_DISCON_CONN = "ratio_disconnected_connected"
RATIO_CONN_TOT = "ratio_connected_total"
RATIO_DISCON_TOT = "ratio_disconnected_total"
STRING_HASH = "string_hash"
RATIO_NUM_SHORTEST_PATH_STRING_HASH = "ratio_shortest_paths_string"

# based on order used in method for determining attributes
attrib_keys = [NUM_EDGES, V_DEGREE, DIAMETER, NUM_SHORTEST_PATH, DISCONNECTED_PAIRS, RATIO_CONN_DISCON, RATIO_DISCON_CONN,
               RATIO_CONN_TOT, STRING_HASH, RATIO_NUM_SHORTEST_PATH_STRING_HASH] # DENSITY]


def compute_kmeans_clusters(graph_db, strategy_context, plot_clusters=False):
    """
    Finds the k-means clusters of the graph db sub-graphs using the
    provided strategy context.
    :param graph_db: the graph database of frequent minimal sub-graphs to cluster
    :return: the sets of similar sub-graphs as determined by k-means alg.
    """
    clusters = None
    patterns = create_patterns_from_graphs(graph_db)
    if len(patterns):
        patterns = normalize(patterns, True)
        k_means = KMeans(n_clusters=strategy_context[NUM_CLUSTERS],
                         init=strategy_context[INIT_TYPE],
                         tol=strategy_context[EPSILON])
        k_means.fit_predict(patterns)
        clusters = get_clusters(k_means)
        # gather all graph string codes and plot clusters
        if plot_clusters:
            bsde_codes = [graph_utils.graph_2_string(graph) for graph in graph_db]
            graph_utils.show_clusters(clusters, bsde_codes, "K-means")
    return clusters


def get_clusters(clusterer):
    clusters = []
    k = clusterer.n_clusters
    labels = clusterer.labels_
    for i in range(k):
        # select only data observations with cluster label == i
        curr_clust = np.where(labels == i)[0].tolist()
        clusters.append(curr_clust)
    return clusters


def create_patterns_from_graphs(graph_db):
    patterns = None
    if len(graph_db) > 0:
        graph_attrib_list = []
        # get all of the graph attributes
        for graph in graph_db:
            graph_attrib_list.append(get_graph_attributes(graph))

        patterns = np.zeros(shape=(len(graph_attrib_list), len(graph_attrib_list[0].keys())))

        i = 0
        # convert graph attribute list into np matrix for k-means clustering
        for graph_attrib_dict in graph_attrib_list:
            j = 0
            for attrib in graph_attrib_dict.keys():
                patterns[i][j] = graph_attrib_dict[attrib]
                j += 1
            i += 1
    return patterns


def get_graph_attributes(graph):
    """
    Compute the attributes of this graph for kmeans clustering.
    :param graph: the simple graph to compute the attributes of
    :return: the attribute dictionary of this graph
    """
    adjacency_matrix = get_adjacency_matrix(graph.nodes, graph.edges)
    graph_attribs = dict()
    # graph_attribs[NUM_VERTICES] = len(graph.nodes)
    graph_attribs[NUM_EDGES] = len(graph.edges)
    graph_attribs[V_DEGREE] = avg_in_degree_rate(adjacency_matrix)
    # graph_attribs[MIN_OUT_DEGREE] = min_out_degree(adjacency_matrix)
    # graph_attribs[MAX_OUT_DEGREE] = max_out_degree(adjacency_matrix)
    # graph_attribs[MIN_IN_DEGREE] = min_in_degree(adjacency_matrix)
    # graph_attribs[MAX_IN_DEGREE] = max_in_degree(adjacency_matrix)
    # get diameter, shortest path count, and number of disconnected node pairs
    graph_attribs[DIAMETER], graph_attribs[NUM_SHORTEST_PATH],\
        graph_attribs[DISCONNECTED_PAIRS] = get_shortest_path_attributes(adjacency_matrix)
    graph_attribs[RATIO_CONN_DISCON] = graph_attribs[NUM_SHORTEST_PATH] / float(graph_attribs[DISCONNECTED_PAIRS])
    graph_attribs[RATIO_DISCON_CONN] = graph_attribs[DISCONNECTED_PAIRS] / float(graph_attribs[NUM_SHORTEST_PATH])
    graph_attribs[RATIO_CONN_TOT] = graph_attribs[NUM_SHORTEST_PATH] / len(graph.nodes) #graph_attribs[NUM_VERTICES]
    graph_attribs[RATIO_DISCON_TOT] = graph_attribs[DISCONNECTED_PAIRS] / len(graph.nodes)#graph_attribs[NUM_VERTICES]
    # graph_attribs[DENSITY] = get_density(graph)
    triples = graph_utils.triples(graph)

    graph_attribs[STRING_HASH] = hash_string(graph_utils.triples_2_string(triples))
    graph_attribs[RATIO_NUM_SHORTEST_PATH_STRING_HASH] = graph_attribs[NUM_SHORTEST_PATH] / graph_attribs[STRING_HASH]
    for graph_attrib in graph_attribs.keys():
        graph_attribs[graph_attrib] = float(graph_attribs[graph_attrib])
    # remember to change attribute keys when this order is changed!
    # this may be implemented in the future but probably not
    # graph_attribs[CLOSENESS_CENT] = get_closeness_centrality(adjacency_matrix)
    return graph_attribs


def hash_string(string_to_hash):
    """
    Compares the provided string to a string of entirely "a's"
    :param string_to_hash: the string to generate a similarity hash for
    :return: the similarity hash for this string (to compare other strings
    with this one)
    """
    a = ''
    for i in range(len(string_to_hash)):
        a = ''.join([a, 'a'])
    return leven_dist(string_to_hash, a)


def avg_in_degree_rate(adj_matrix):
    """
    Compute the mean in-degree percentage for the matrix.
    In-degree is the same as out degree in this case.
    :param adj_matrix: the matrix to compute in degree over
    :return: the avg percentage of in-degree in the entire matrix
    """
    tot_in_degree = []
    for col in range(len(adj_matrix[0])):
        in_degree_ = 0
        for row in range(len(adj_matrix)):
            in_degree_ += adj_matrix[row][col]
        tot_in_degree.append(in_degree_/float(len(adj_matrix)))
    return sum(tot_in_degree) / float(len(tot_in_degree))


def min_in_degree(adj_matrix):
    """
    Compute the minimum in degree for te matrix.
    :param adj_matrix: the matrix to compute in-degree over
    :return: the minimum in-degree of the entire matrix
    """
    tot_in_degree = []
    for col in range(len(adj_matrix[0])):
        in_degree_ = 0
        for row in range(len(adj_matrix)):
            in_degree_ += adj_matrix[row][col]
        tot_in_degree.append(in_degree_)
    return min(tot_in_degree)


def max_in_degree(adj_matrix):
    """
    Compute the maximum in degree for te matrix.
    :param adj_matrix: the matrix to compute in-degree over
    :return: the maximum in-degree of the entire matrix
    """
    tot_in_degree = []
    for col in range(len(adj_matrix[0])):
        in_degree_ = 0
        for row in range(len(adj_matrix)):
            in_degree_ += adj_matrix[row][col]
        tot_in_degree.append(in_degree_)
    return max(tot_in_degree)


def min_out_degree(adj_matrix):
    """
    Compute the minimum out degree of all vertices by summing their rows in the matrix.
    :param adj_matrix: the adjacency matrix to use
    :return: the minimum out degree of all nodes in the matrix
    """
    tot_out_degree = []
    for row in range(len(adj_matrix)):
        out_degree_ = 0
        for col in range(len(adj_matrix[row])):
            out_degree_ += adj_matrix[row][col]
        tot_out_degree.append(out_degree_)
    return min(tot_out_degree)


def max_out_degree(adj_matrix):
    """
    Compute the maximum out degree of all vertices by summing their rows in the matrix.
    :param adj_matrix: the adjacency matrix to use
    :return: the maximum out degree of all nodes in the matrix
    """
    tot_out_degree = []
    for row in range(len(adj_matrix)):
        out_degree_ = 0
        for col in range(len(adj_matrix[row])):
            out_degree_ += adj_matrix[row][col]
        tot_out_degree.append(out_degree_)
    return max(tot_out_degree)


def get_shortest_path_attributes(adj_matrix):
    """
    Gets the diameter of the graph and the number of shortest paths as well as
    the number of unconnected nodes.
    The diameter d of a graph is the maximum eccentricity of any vertex in the graph.
    The diameter is the length of the shortest path between the most distanced nodes.
    To determine the diameter of a graph, find the longest of the shortest paths between each pair of vertices.
    The longest of any of these paths is the diameter of the graph.
    """
    shortest_path_matrix = graph_shortest_path(adj_matrix, directed=True, method='auto')
    # find the diameter of the graph
    diameter = shortest_path_matrix.max()

    # find num of shortest paths (lengths non-zero)
    num_shortest_paths = np.count_nonzero(shortest_path_matrix)

    # find num of disconnected node pairs
    num_disconnected_node_pairs = shortest_path_matrix.shape[0] * shortest_path_matrix.shape[1] - \
                                  shortest_path_matrix.shape[0] - num_shortest_paths

    return diameter, num_shortest_paths, num_disconnected_node_pairs


def get_density(graph):
    """
    Calculates the density of the provided graph
    with nodes and edges.
    """
    num_vertices = len(graph.nodes)
    num_edges = len(graph.edges)
    return 2.0 * num_edges / (num_vertices * (num_vertices - 1))


def get_closeness_centrality(adj_matrix):
    pass
